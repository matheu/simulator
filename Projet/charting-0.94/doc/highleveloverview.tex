\subsection{High-Level Class Overview}

The whole library is centered around the \texttt{DefaultChart} class.
 This is the point to start when you create a new chart but also if you
 want to understand how things work.
 A chart contains several parts as depicted below.

\begin{figure}[h]
\centering
\includegraphics[width=150mm]{jopenchartoverview}
\caption{The basic structure of the charting classes.}
\end{figure}

The \texttt{Title} class just encapsulates the \texttt{Chart}'s title as the name implies.
 Like the \texttt{Legend} this class extends \texttt{AbstractRenderer} which will be explained
 later on.
 The \texttt{Legend} is the second one of the easy classes.
 Again it's pretty obvious what it is, the \texttt{Chart}'s legend of course.
 The last three ones - the \texttt{ChartDataModel}, the \texttt{CoordSystem} and the \texttt{ChartRenderer}
 Architecture - will be explained in more detail later on.
 As you can see in the picture, the \texttt{Legend} and the \texttt{ChartDataModel} have a
 special connection which is the \texttt{RowColorModel}.
 This class encapsulates the connection between data sets and colors.

\subsubsection{The ChartDataModel Classes}

You can get a first grasp of the model architecture if you take a look at
 the \texttt{ChartDataModel} interface.
 It looks pretty similar to the \texttt{TableDataModel} from the Swing package.
 Superficially, this is right.
 For a moment, just think of the \texttt{ChartDataModel} as a simple table.
 The table columns define the x-axis values while every table row is a new
 data set:

\begin{table}[h]
\centering

\begin{tabular}{|l|c|c|c|c|c|}\hline
\textit{x-axis values} & 0 & 1 & 2 & 3 & 4 \\ \hline\hline
\textit{Data Set 1} & 0 & 2 & 4 & 6 & 8 \\ \hline
\textit{Data Set 2} & 0 & 1 & 4 & 9 & 16 \\ \hline
\end{tabular}

%\caption{A simple table showing two data sets and the corresponding x-axis values in the first row.}
\end{table}

As you can see, \textit{DataSet 1} defines the linear function  $f(x)=2x$
and \textit{DataSet 2} defines the function $f(x)=x^{2}$.
 You find all the usual table methods in the interface.
 You can read a certain value with \texttt{getValue(int set, int index)}, 
read a certain column (ie x-axis) value, determine the length of a certain
 data set and get the title of a data set (eg \textit{Data Set 1} in the table above).
 There is another method called \texttt{getChartDataModelConstraints(int axis)}.
 This returns a \texttt{ChartDataModelConstraints} object which you will easily understand 
 if you look at the \texttt{ChartDataModelConstraints} interface.
 This interface just contains four methods which provide access to the minimal
 and maximal x and y-axis values.
 This is necessary for rendering the coordinate system.
\par
 Of course, the whole thing wouldn't be very flexible if the data model was
 really a table.
 You will find an extra \texttt{DataSet} interface and a \texttt{DefaultDataSet} implementation
 in the model package and those two encapsulate the data sets.
 This way, a \texttt{ChartDataModel} can contain multiple \texttt{DataSet}s with different
 length and they can even have their own x-axis values.
 At least, this is true for data models with numerical x-axis values where
 you can paint every arbitrary value as long as you know the \texttt{ChartDataModelConstraints}, 
basically because numbers are automatically ordered in a mathematical
 sense.
 This is different for data models with non-numerical x-axis values, eg
 usual bar charts fall in this category.
 In the next release it will be possible to create such charts using data
 sets with different lengths, as long as you provide an ordering for the
 different x-axis values.

\par
There are two different \texttt{ChartDataModel} implementations which meet those
 two different requirements.
 \texttt{DefaultChartDataModel} implements the data model for the use with numerical
 x-axis values while \texttt{ObjectChartDataModel} has arbitrary \texttt{Object}s as x-axis
 values.
 They could be \texttt{String}s or \texttt{Date}s for example.
 Both implementations are pretty straightforward, except computing the 
\texttt{ChartDataModelConstraints} which will be explained in detail in the Algorithms part
 of this guide.

\subsubsection{The CoordSystem Class}

The coordinate system implementation is the core class of all the later
 rendering stuff.
 That's why this class has to do a bit of computing.
 It needs to know the \texttt{ChartDataModelConstraints} and it's own bounds and
 then it can compute the placing of the axes, the point (0, 0) and the pixel
 / point ratio.
 Besides that, this class contains a lot of rendering code for painting
 the axes, the ticks, labels etc.
 pp.
 All this stuff has been externalized to the \texttt{CoordSystemUtilities} class
 to make the whole shebang more readible.
 The most important part is transforming the user space coordinates of the
 \texttt{ChartDataModel} to the device space coordinates used to paint in an image
 or on the screen.
 This is done using the \texttt{AffineTransform} class of the \texttt{java.awt.geom} package.
 This class defines a multiplication matrix which is used by the \texttt{ChartRenderer}s
 to transform those coordinates.
 The mathematical part is as usual explained in the Algorithms part.
 It is possible to chain \texttt{AffineTransform}s which will be used in the Swing
 chart classes to move charts around on the screen.
 
\par
Another thing you may wonder about is the second y-axis.
 It is not really implemented yet, but sometimes it is desirable to combine
 two data sets in one chart although their y-axis values have different
 units.
 You probably know charts which visualize the development of processor speed
 and transistors per processor.
 Those charts need to y-axis because processor speed and transistors per
 processor are different units.
 
\subsubsection{The Render Architecture}

Now I finally come to the interesting part which still needs some work.
 The place to start is the \texttt{Renderer} interface.
 It defines only a few methods for setting the bounds, getting the preferred
 size and finally rendering what has to be rendered.
 This interface is not incredibly interesting of course.
 It gets more interesting when you look at the \texttt{AbstractRenderer} and the
 \texttt{AbstractChartRenderer}.
 The former implements the boring getter and setter method for the bounds
 object but it also provides an implementation for the render method.
 Additionally, it defines a \texttt{paintDefault(Graphics2D g)} method.
 So what is that? Well, the default render method takes care for the fact,
 that the assigned bounds to a render object might be smaller than the preferred
 size.
 In this case it creates an offscreen image in which the actual renderer
 will paint and which will be scaled down afterwards to the size of the
 bounds object.
 This means that every \texttt{Renderer} implementation only has to provide an implementation 
for the \texttt{paintDefault(Graphics2D g)} method which can always assume the preferred size
 and to start painting at coordinates (0, 0).
 
\par
The latter class I mentioned - \texttt{AbstractChartRenderer} - provides default
 implementations for \texttt{ChartRenderer}s.
 This includes mainly several interface methods like getters and setters
 for the \texttt{CoordSystem} etc.
 The only method left to implement in a real \texttt{ChartRenderer} is the \texttt{render(Graphics2D g)} method.
 These two abstract classes simplify creating new \texttt{Renderer}s a lot.
 Have fun, there are a lot of \texttt{ChartRenderer}s missing.

\subsubsection{The Easy Ones: Titles and Legends}

Maybe it's a little bit surprising, but in fact, both \texttt{Title} and \texttt{Legend} objects
 are themselves \texttt{Renderer}s.
 This is pretty handy if you think of the automatic scaling capabilities
 in the \texttt{AbstractRenderer} class.
 Besides that, there's not much to say about them.
 Just look at the code, it's pretty straightforward.

\subsubsection{The Interesting Ones: ChartRenderers}


The more interesting renderers are the real \texttt{ChartRenderer}s.
 As described above, all they do is to implement the render method.
 This isn't extraordinarily exciting either.
 In fact, if you compare the \texttt{Renderer}s you'll find that they are all very
 similar.
 They all iterate over all \texttt{DataSet}s in the \texttt{ChartDataModel} and somehow they
 all paint the data found in there in a small iteration loop.
