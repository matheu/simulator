package tuto2;
import sim.engine.*;
import sim.display.*;
import sim.portrayal.grid.*;

import java.awt.*;

import javax.swing.*;

public class Interfacemason extends GUIState
{

	public Display2D display;
    public JFrame displayFrame;

    SparseGridPortrayal2D particlesPortrayal = new SparseGridPortrayal2D();
    FastValueGridPortrayal2D trailsPortrayal = new FastValueGridPortrayal2D("Trail");

    public static void main(String[] args)
        {
        new Interfacemason().createController();
        }
    
    public Interfacemason() { super(new test2(System.currentTimeMillis())); }
    
    public Interfacemason(SimState state) { super(state); }
    
    public static String getName() { return "Tutorial3: Particles"; }
    
    public static Object getInfo()
        {
        return "<H2>Tutorial3</H2><p>An odd little particle-interaction example.";
        }
    
    public void quit()
        {
        super.quit();
        
        if (displayFrame!=null) displayFrame.dispose();
        displayFrame = null;  // let gc
        display = null;       // let gc
        }

    public void start()
        {
        super.start();
        setupPortrayals();
        // this time, we'll call display.reset() and display.repaint() in setupPortrayals()
        }
    
    public void load(SimState state)
        {
        super.load(state);
        setupPortrayals();
        // likewise...
        }

    public void setupPortrayals()
    {
    // tell the portrayals what to
    // portray and how to portray them
    trailsPortrayal.setField(
    	((test2)state).trails);
    trailsPortrayal.setMap(
        new sim.util.gui.SimpleColorMap(
	    	0.0,1.0,Color.black,Color.white));
    particlesPortrayal.setField(((test2)state).particles);
    particlesPortrayal.setPortrayalForAll(
        new sim.portrayal.simple.OvalPortrayal2D(Color.green) );
               
    // reschedule the displayer
    display.reset();
            
    // redraw the display
    display.repaint();
    }
    
    public void init(Controller c)
    {
    super.init(c);
    display = new Display2D(400,400,this);
    displayFrame = display.createFrame();
    c.registerFrame(displayFrame);
    displayFrame.setVisible(true);
    display.setBackdrop(Color.black);
    display.attach(trailsPortrayal,"Trails");
    display.attach(particlesPortrayal,"Particles");
    }

    
}
