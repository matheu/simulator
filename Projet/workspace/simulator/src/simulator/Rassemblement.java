package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Double2D;

public class Rassemblement extends SimplePortrayal2D
{
	private static final long serialVersionUID = 1;
	private Double2D p;
	public String id;

	
	public Rassemblement()
	{
		p = new Double2D();
		id="";
	}
	
	public Rassemblement(Double2D c, String id)
	{
		p = new Double2D();
		p = c;
		this.id=id;
	}
	
	public Rassemblement(double x, double y, String id)
	{
		p = new Double2D(x,y);
		this.id=id;
	}


	public Double2D getCoord()
	{
		return p;
	}
	public String getId()
	{
		return id;
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		final double width = info.draw.width*10;
		final double height = info.draw.height*10;
		
		Color c=Color.yellow;
		if(simulateur.afficheEtage>0)
			c=c.darker().darker().darker().darker().darker();
		graphics.setColor(c);
		
		final int x = (int)(info.draw.x - width / 2.0);
		final int y = (int)(info.draw.y - height / 2.0);
		final int w = (int)(width);
		final int h = (int)(height);
		
		// draw centered on the origin
		graphics.drawOval(x,y,w,h);
		graphics.setColor(c.darker().darker().darker().darker());
		graphics.fillOval(x+1,y+1,w-2,h-2);
	}
}
