package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Double2D;

public class DepartCatastrophe extends SimplePortrayal2D
{
	private static final long serialVersionUID = 1;
	private Double2D p;
	public int etage;
	private String type;
	public String id;

	
	public DepartCatastrophe()
	{
		p = new Double2D();
		etage=0;
		type="feu";
		id="";
	}
	
	public DepartCatastrophe(int etage, Double2D c, String t, String id)
	{
		this.etage=etage;
		p = new Double2D();
		p = c;
		type=t;
		this.id=id;
	}
	
	public DepartCatastrophe(int etage, double x, double y, String t, String id)
	{
		this.etage= etage;
		p = new Double2D(x,y);
		type=t;
		this.id=id;
	}


	public int getEtage()
	{
		return etage;
	}
	public Double2D getCoord()
	{
		return p;
	}
	public String getType()
	{
		return type;
	}
	public String getId()
	{
		return id;
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		if (simulateur.afficheDepartCatastrophe && (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1))
		{
			final double width = info.draw.width*1.0;
			final double height = info.draw.height*1.0;
			
			Color c=Color.orange;
			if(etage==simulateur.afficheEtage-1){
		    	c=c.darker().darker().darker().darker().darker();
		    }
			
			final int x = (int)(info.draw.x - width / 2.0);
			final int y = (int)(info.draw.y - height / 2.0);
			final int w = (int)(width);
			final int h = (int)(height);
			
			// draw centered on the origin
			graphics.setColor(c);
			graphics.fillOval(x,y,w,h);
		}
	}
}
