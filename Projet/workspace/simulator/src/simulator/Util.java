package simulator;

import sim.util.Double2D;

public class Util
{
	public static double distanceManhattan(Double2D directionA, Double2D directionB)
	{
		return Math.abs(directionB.x-directionA.x)+Math.abs(directionB.y-directionA.y);
	}
	public static double distanceEuclidienne(Double2D directionA, Double2D directionB)
	{
		return Math.sqrt(Math.pow(directionB.x-directionA.x, 2)+Math.pow(directionB.y-directionA.y, 2));
	}
}
