package simulator;

import org.jdom2.*;
import org.jdom2.input.*;

import java.io.File;
import java.util.List;
import java.util.Iterator;
import java.util.Vector;

public class JDOM
{
	static org.jdom2.Document document;
	static Element racine;
	
	public static Vector<Object> chargerBatiments()
	{
		SAXBuilder sxb = new SAXBuilder();
		try
		{
			document = sxb.build(new File("src/batiments.xml"));
		}
		catch(Exception e){}
		racine = document.getRootElement();
		
		Vector<Object> coords = new Vector<Object>();
		List<Element> listCoins = racine.getChildren("coins").get(0).getChildren("coin");
		List<Element> listPieces = racine.getChildren("piece");

		coords.add(Integer.parseInt(racine.getAttributeValue("etages")));
		coords.add(Integer.parseInt(racine.getAttributeValue("tailleX")));
		coords.add(Integer.parseInt(racine.getAttributeValue("tailleY")));
		coords.add(Integer.parseInt(racine.getAttributeValue("sol")));
		
		Iterator<Element> i = listPieces.iterator();
		while(i.hasNext())
		{
			Element courant = (Element)i.next();
			List<Element> murs = courant.getChildren("mur");
			Iterator<Element> iMur = murs.iterator();
			while(iMur.hasNext())
			{
				Element courantMur = (Element)iMur.next();
				int j=0;
				while(j<listCoins.size() && !listCoins.get(j).getAttributeValue("id").equals(courantMur.getAttributeValue("p1")))
				{
					j++;
				}
				int j2=0;
				while(j2<listCoins.size() && !listCoins.get(j2).getAttributeValue("id").equals(courantMur.getAttributeValue("p2")))
				{
					j2++;
				}
				
				coords.add(new Mur(
					new Coin(
						Integer.parseInt(listCoins.get(j).getAttributeValue("z")),
						Double.parseDouble(listCoins.get(j).getAttributeValue("x")),
						Double.parseDouble(listCoins.get(j).getAttributeValue("y"))
					),
					new Coin(
						Integer.parseInt(listCoins.get(j).getAttributeValue("z")),
						Double.parseDouble(listCoins.get(j2).getAttributeValue("x")),
						Double.parseDouble(listCoins.get(j2).getAttributeValue("y"))
					)
				));
			}
			
			
			List<Element> portes = courant.getChildren("porte");
			Iterator<Element> iPorte = portes.iterator();
			while(iPorte.hasNext())
			{
				Element courantPorte = (Element)iPorte.next();
				
				coords.add(new Porte(
					Integer.parseInt(courantPorte.getAttributeValue("z")),
					Double.parseDouble(courantPorte.getAttributeValue("x")),
					Double.parseDouble(courantPorte.getAttributeValue("y")),
					Double.parseDouble(courantPorte.getAttributeValue("angle")),
					String.valueOf(courantPorte.getAttributeValue("id"))
				));
			}
			
			
			List<Element> escaliers = courant.getChildren("escalier");
			Iterator<Element> iEscalier = escaliers.iterator();
			while(iEscalier.hasNext())
			{
				Element courantEscalier = (Element)iEscalier.next();

				coords.add(new Escalier(
					Integer.parseInt(courantEscalier.getAttributeValue("z1")),
					Double.parseDouble(courantEscalier.getAttributeValue("x1")),
					Double.parseDouble(courantEscalier.getAttributeValue("y1")),
					Double.parseDouble(courantEscalier.getAttributeValue("x2")),
					Double.parseDouble(courantEscalier.getAttributeValue("y2")),
					(Integer.parseInt(courantEscalier.getAttributeValue("z1"))<Integer.parseInt(courantEscalier.getAttributeValue("z2"))),
					String.valueOf(courantEscalier.getAttributeValue("id"))
				));
				coords.add(new Escalier(
					Integer.parseInt(courantEscalier.getAttributeValue("z2")),
					Double.parseDouble(courantEscalier.getAttributeValue("x2")),
					Double.parseDouble(courantEscalier.getAttributeValue("y2")),
					Double.parseDouble(courantEscalier.getAttributeValue("x1")),
					Double.parseDouble(courantEscalier.getAttributeValue("y1")),
					(Integer.parseInt(courantEscalier.getAttributeValue("z1"))>Integer.parseInt(courantEscalier.getAttributeValue("z2"))),
					String.valueOf(courantEscalier.getAttributeValue("id"))
				));
			}
			
			
			List<Element> spawns = courant.getChildren("spawn");
			Iterator<Element> iSpawn = spawns.iterator();
			while(iSpawn.hasNext())
			{
				Element courantSpawn = (Element)iSpawn.next();
				
				coords.add(new Spawn(
					Integer.parseInt(courantSpawn.getAttributeValue("z")),
					Double.parseDouble(courantSpawn.getAttributeValue("x")),
					Double.parseDouble(courantSpawn.getAttributeValue("y")),
					String.valueOf(courantSpawn.getAttributeValue("id")),
					1.0
				));
			}
			
			
			List<Element> rassemblements = courant.getChildren("rassemblement");
			Iterator<Element> iRassemblement = rassemblements.iterator();
			while(iRassemblement.hasNext())
			{
				Element courantRassemblement = (Element)iRassemblement.next();
				
				coords.add(new Rassemblement(
					Double.parseDouble(courantRassemblement.getAttributeValue("x")),
					Double.parseDouble(courantRassemblement.getAttributeValue("y")),
					String.valueOf(courantRassemblement.getAttributeValue("id"))
				));
			}
			
			
			List<Element> panneauxSortie = courant.getChildren("panneauSortie");
			Iterator<Element> iPanneauSortie = panneauxSortie.iterator();
			while(iPanneauSortie.hasNext())
			{
				Element courantPanneauSortie = (Element)iPanneauSortie.next();
				
				coords.add(new PanneauSortie(
					Integer.parseInt(courantPanneauSortie.getAttributeValue("z")),
					Double.parseDouble(courantPanneauSortie.getAttributeValue("x")),
					Double.parseDouble(courantPanneauSortie.getAttributeValue("y")),
					Double.parseDouble(courantPanneauSortie.getAttributeValue("angle")),
					String.valueOf(courantPanneauSortie.getAttributeValue("id"))
				));
			}
			
			
			List<Element> departsCatastrophes = courant.getChildren("departCatastrophe");
			Iterator<Element> iDepartCatastrophe = departsCatastrophes.iterator();
			while(iDepartCatastrophe.hasNext())
			{
				Element courantDepartCatastrophe = (Element)iDepartCatastrophe.next();
				
				coords.add(new DepartCatastrophe(
					Integer.parseInt(courantDepartCatastrophe.getAttributeValue("z")),
					Double.parseDouble(courantDepartCatastrophe.getAttributeValue("x")),
					Double.parseDouble(courantDepartCatastrophe.getAttributeValue("y")),
					String.valueOf(courantDepartCatastrophe.getAttributeValue("type")),
					String.valueOf(courantDepartCatastrophe.getAttributeValue("id"))
				));
			}
		}
		return coords;
	}
}
