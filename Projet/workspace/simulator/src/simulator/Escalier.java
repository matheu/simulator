package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Double2D;

public class Escalier extends SimplePortrayal2D
{
	private static final long serialVersionUID = 1;
	private Double2D depars;
	private Double2D arrive;
	private boolean monte;
	public int etage;
	public String id;

	//Permet de d�finir la charge de capacit� de l'escalier ainsi que le temps n�cessaire en ticks pour le traverser
	//TODO : Les rajouter dans les constructeurs
	//TODO : Mettre un pointeur vers l'instance de l'escalier dans l'�tage d'arriv�e, afin de garder la coh�rence des donn�es pour l'utilisation
	public int tempsTraversee;
	public int chargeMax;
	public int chargeActuelle;


	public Escalier()
	{
		depars = new Double2D();
		arrive = new Double2D();
		monte = false;
		etage = 0;
		id="";
		tempsTraversee = 50;
		chargeMax = 2;
		chargeActuelle = 0;
	}

	/**
	 * 
	 * @param depars emplacement de l'escalier
	 * @param arrive arriver de l'escalier
	 * @param monte vrai si l'escalier fais monte sinon false
	 */
	public Escalier(int etage, Double2D depars, Double2D arrive, boolean monte, String id)
	{
		this.depars = new Double2D();
		this.depars = depars;
		this.arrive = new Double2D();
		this.arrive = arrive;
		this.monte = monte;
		this.id=id;
		tempsTraversee = 50;
		chargeMax = 2;
		chargeActuelle = 0;
	}

	/**
	 * 
	 * @param x emplacement de l'escalier
	 * @param y emplacement de l'escalier
	 * @param x1 arriver de l'escalier
	 * @param y1 arriver de l'escalier
	 * @param  
	 * @param monte vrai si l'escalier fais monte sinon false
	 */
	public Escalier(int etage, double x, double y, double x1, double y1, boolean monte, String id)
	{
		this.etage = etage;
		depars = new Double2D(x,y);
		arrive = new Double2D(x1,y1);
		this.monte = monte;
		this.id=id;
		tempsTraversee = 25;
		chargeMax = 5;
		chargeActuelle = 0;
	}

	public int getEtage()
	{
		return etage;
	}
	public Double2D getCoord()
	{
		return depars;
	}

	public Double2D getArrive()
	{
		return arrive;
	}

	public boolean isMonte()
	{
		return monte;
	}
	public String getId()
	{
		return id;
	}

	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		if (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1)
		{
			final double width = info.draw.width*1.0;
			final double height = info.draw.height*1.0;

			Color c=Color.white;
			if(etage==simulateur.afficheEtage-1){
				c=c.darker().darker().darker().darker().darker();
			}
			graphics.setColor(c);

			final int x = (int)(info.draw.x - width / 2.0);
			final int y = (int)(info.draw.y - height / 2.0);
			final int w = (int)(width);
			final int h = (int)(height);

			// draw centered on the origin
			graphics.drawRect(x,y,w,h);
		}
	}
}
