package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Double2D;

public class Coin extends SimplePortrayal2D
{
	private static final long serialVersionUID = 1;
	private Double2D p;
	private int etage;
	
	public Coin()
	{
		p = new Double2D();
		etage= 0;
	}
	
	public Coin(int etage, Double2D c)
	{
		p = new Double2D();
		p = c;
		this.etage= etage;
	}
	
	public Coin(int etage,double x, double y)
	{
		p = new Double2D(x,y);
		this.etage= etage;
	}

	public Coin(int etage2) {
		p = new Double2D();
		this.etage= etage2;
	}

	public int getEtage()
	{
		return etage;
	}
	public Double2D getCoord()
	{
		return p;
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		if (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1)
			if (simulateur.afficheMur)
			{
				final double width = info.draw.width*1.0;
				final double height = info.draw.height*1.0;
				
				Color c=Color.yellow;
				if(etage==simulateur.afficheEtage-1){
			    	c=c.darker().darker().darker().darker().darker();
			    }
				graphics.setColor(c);
				
				final int x = (int)(info.draw.x - width / 2.0);
				final int y = (int)(info.draw.y - height / 2.0);
				final int w = (int)(width);
				final int h = (int)(height);
				
				// draw centered on the origin
				graphics.fillRect(x,y,w,h);
			}
	}
}
