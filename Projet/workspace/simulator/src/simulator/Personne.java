package simulator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Bag;
import sim.util.Double2D;

public class Personne extends SimplePortrayal2D implements Steppable
{
	private static final long serialVersionUID = 1;
	public Double2D direction;
	public Double2D currentDirection;
	public Double2D objective;
	public int etage;
	public boolean sorti;
	public boolean collision;
	public boolean random;
	public Double vitesse;
	public Double vitesseBase;
	public Double distanceParcouru;
	public int collisionType;
	public int nbTick;
	public Double2D forceMurs;
	public double distancePerceptionInstant;
	public boolean isLocked;
	public boolean hasObjective;
	public int objectiveDate;
	public Double2D col;
	public double wiggleFactor;
	public double panique;
	public boolean mort = false;
	public int getNbTick(){ return nbTick ;}
	public Double getDistanceParcouru(){return distanceParcouru;}
	public Double getVitesse() { return vitesse; }
	public Double2D getCol() { return col; }
	public Double2D getObjective(){return objective;}
	//La distance de perception de l'agent
	//TODO: La faire varier a l'initialisation (Certains voient mieux que d'autres)
	public Double distancePerception;
	//Les perceptions de l'agent
	public Bag perceptMur;
	public Bag perceptPersonne;
	public Bag perceptEscalier;
	public Bag perceptPorte;
	public Bag perceptPanneau;
	public Bag perceptRassemblement;
	public Bag perceptFeu;
	
	public Double2D getForceMurs(){return forceMurs;}
	public int vie;
	public int maxVie;
	public int getVie(){return vie;}
	
	//Le choix actuel
	public Choix choix;
	public Choix getChoix(){return choix;}
	
	//Permet de d�layer les sorties d'escalier
	//TODO: Rajouter un pointeur vers l'escalier actuel afin de pouvoir modifier son �tat en sortant
	public boolean dansEscalier;
	public int tickEscalier;
	public Escalier currentEscalier;
	public double getWiggleFactor(){return wiggleFactor;};
	//public void setVitesse(Double vitesse) { this.vitesse = vitesse; }
	public Color c;
	public Double2D getDirection() { return direction; }
	//public boolean getRandomize() { return random; }
	//public void setRandomize(boolean val) { random = val; }

	public Personne(Double vitesseBase, int etage, Double perception, int vie)
	{
		this.etage = etage;
		nbTick = 0;
		sorti = false;
		distanceParcouru = 0.0;
		direction = new Double2D(1, 0);
		col = new Double2D(-1, -1);
		currentDirection = new Double2D(1, 0);
		objective = new Double2D(1,0);
		hasObjective = false;
		random=true;
		this.vitesseBase = vitesseBase;
		isLocked = false;
		distancePerception = perception;
		choix = Choix.ALEATOIRE;
		this.maxVie = vie;
		this.vie = maxVie;
		wiggleFactor = 0.01;
	}

	public void clearPercept()
	{
		perceptMur.clear();
		perceptPersonne.clear();
		perceptEscalier.clear();
		perceptPorte.clear();
		perceptPanneau.clear();
		perceptRassemblement.clear();
		perceptFeu.clear();
	}
	
	public void mergePercept()
	{
		perceptMur.addAll(perceptPorte);
		perceptPorte.clear();
	}
	
	public boolean aDroite(Double2D p1, Double2D p2, Double2D p3)
	{
		return ((p2.x-p1.x)*(p3.y-p1.y)-(p2.y-p1.y)*(p3.x-p1.x)) < 0;
	}
	
	public boolean deChaqueCote(Double2D p1, Double2D p2, Double2D p3, Double2D p4)
	{
		return (aDroite(p1,p2,p3) != aDroite(p1, p2, p4)); 
	}
	
	public boolean intersect(Double2D p1, Double2D p2, Double2D p3, Double2D p4)
	{
		return (deChaqueCote(p1, p2, p3, p4) && deChaqueCote(p3, p4, p1, p2));
	}
	
	public void realPerception(simulateur simul)
	{
		Double2D pos = simul.gens.get(etage).getObjectLocation(this);
		Double2D objet;
		Double2D mur;
		Double2D m1 = new Double2D(1,1);
		Double2D m2 = new Double2D(1,-1);
		Bag coll = new Bag();
		
		coll.addAll(perceptPorte);
		for(int i = 0; i < perceptMur.size(); i++)
		{
					mur = simul.gens.get(etage).getObjectLocation(perceptMur.get(i));
					//Portes
					for(int j = 0; j < perceptPorte.size(); j++)
					{
						objet = simul.gens.get(etage).getObjectLocation(perceptPorte.get(j));
						if(intersect(pos, objet, mur.add(m1), mur.add(m2)))
						{
							perceptPorte.remove(j);
							j--;
						}
						else if(intersect(pos, objet, mur.add(m1), mur.subtract(m2)))
						{
							perceptPorte.remove(j);
							j--;
						}
						else if(intersect(pos, objet, mur.subtract(m1), mur.add(m2)))
						{
							perceptPorte.remove(j);
							j--;
						}
						else if(intersect(pos, objet, mur.subtract(m1), mur.subtract(m2)))
						{
							perceptPorte.remove(j);
							j--;
						}
					}
		}
		coll.addAll(perceptMur);
		for(int i = 0; i < /*perceptMur*/coll.size(); i++)
		{
			mur = simul.gens.get(etage).getObjectLocation(/*perceptMur*/coll.get(i));
			//Panneau
			for(int j = 0; j < perceptPanneau.size(); j++)
			{
				objet = simul.gens.get(etage).getObjectLocation(perceptPanneau.get(j));
				if(intersect(pos, objet, mur.add(m1), mur.add(m2)))
				{
					perceptPanneau.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.add(m1), mur.subtract(m2)))
				{
					perceptPanneau.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.add(m2)))
				{
					perceptPanneau.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.subtract(m2)))
				{
					perceptPanneau.remove(j);
					j--;
				}
			}
			
			//Escalier
			for(int j = 0; j < perceptEscalier.size(); j++)
			{
				objet = simul.gens.get(etage).getObjectLocation(perceptEscalier.get(j));
				if(intersect(pos, objet, mur.add(m1), mur.add(m2)))
				{
					perceptEscalier.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.add(m1), mur.subtract(m2)))
				{
					perceptEscalier.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.add(m2)))
				{
					perceptEscalier.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.subtract(m2)))
				{
					perceptEscalier.remove(j);
					j--;
				}
			}
			
			//Sortie
			for(int j = 0; j < perceptRassemblement.size(); j++)
			{
				objet = simul.gens.get(etage).getObjectLocation(perceptRassemblement.get(j));
				if(intersect(pos, objet, mur.add(m1), mur.add(m2)))
				{
					perceptRassemblement.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.add(m1), mur.subtract(m2)))
				{
					perceptRassemblement.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.add(m2)))
				{
					perceptRassemblement.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.subtract(m2)))
				{
					perceptRassemblement.remove(j);
					j--;
				}
			}
			
			
			
			//Gens
			for(int j = 0; j < perceptPersonne.size(); j++)
			{
				objet = simul.gens.get(etage).getObjectLocation(perceptPersonne.get(j));
				if(intersect(pos, objet, mur.add(m1), mur.add(m2)))
				{
					perceptPersonne.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.add(m1), mur.subtract(m2)))
				{
					perceptPersonne.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.add(m2)))
				{
					perceptPersonne.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.subtract(m2)))
				{
					perceptPersonne.remove(j);
					j--;
				}
			}

			//Feu
			for(int j = 0; j < perceptFeu.size(); j++)
			{
				objet = simul.gens.get(etage).getObjectLocation(perceptFeu.get(j));
				if(intersect(pos, objet, mur.add(m1), mur.add(m2)))
				{
					perceptFeu.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.add(m1), mur.subtract(m2)))
				{
					perceptFeu.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.add(m2)))
				{
					perceptFeu.remove(j);
					j--;
				}
				else if(intersect(pos, objet, mur.subtract(m1), mur.subtract(m2)))
				{
					perceptFeu.remove(j);
					j--;
				}
			}
		}
	}
	private void degatsFeu(simulateur simul)
	{
		Double2D pos = simul.gens.get(etage).getObjectLocation(this);
		for(int i = 0; i < perceptFeu.size(); i++)
		{
			if(!((Feu)(perceptFeu.get(i))).estFumee())
				if(pos.distance(simul.gens.get(etage).getObjectLocation(perceptFeu.get(i)))<5)
					vie--;
		}
	}
	
	private boolean isVisible(simulateur simul, Double2D posAgent, Double2D posObjet)
	{
		Double2D mur;
		Double2D m1 = new Double2D(0.51,0.51);
		Double2D m2 = new Double2D(0.51,-0.51);
		for(int i = 0; i < perceptMur.size(); i++)
		{
			mur = simul.gens.get(etage).getObjectLocation(perceptMur.get(i));
			if(intersect(posAgent, posObjet, mur.add(m1), mur.add(m2)))
			{
				return false;
			}
			else if(intersect(posAgent, posObjet, mur.add(m1), mur.subtract(m2)))
			{
				return false;
			}
			else if(intersect(posAgent, posObjet, mur.subtract(m1), mur.add(m2)))
			{
				return false;
			}
			else if(intersect(posAgent, posObjet, mur.subtract(m1), mur.subtract(m2)))
			{
				return false;
			}
		}
		return true;
	}
	
	private Double2D rectifierDirection(simulateur simul,Double2D dir)
	{
		Double2D pos = simul.gens.get(etage).getObjectLocation(this);
		Double2D directionInstantanee = pos.add(direction.multiply(5));
		int i = 5;
		int j = 0;
		while(!isVisible(simul, pos, directionInstantanee))
		{
			dir = dir.rotate(Math.toRadians(i));
			if(i < 0)
				i = -(i-5);
			else
				i = -(i+5);
			directionInstantanee = pos.add(dir.multiply(3));
			if(++j > 71)
			{
				//System.out.println("I WANT TO BREAK FREE " + pos);
				return dir;
			}
		}
		return dir;
	}
	
	private void perception(simulateur simul)
	{
		Double2D pos = simul.gens.get(etage).getObjectLocation(this);
		//distancePerceptionInstant = wiggleFactor>0.1?distancePerception*(wiggleFactor*2.5-0.5):distancePerception;
		distancePerceptionInstant = wiggleFactor>0.1?distancePerception*(1-wiggleFactor):distancePerception;
		Bag b = simul.gens.get(etage).getNeighborsExactlyWithinDistance(pos, distancePerceptionInstant);
		perceptMur = new Bag();
		perceptPersonne = new Bag();
		perceptEscalier = new Bag();
		perceptPorte = new Bag();
		perceptPanneau = new Bag();
		perceptRassemblement = new Bag();
		perceptFeu = new Bag();
		for(int i=0; i<b.size(); i++)
		{
			if(b.get(i).getClass().equals(Personne.class))
				perceptPersonne.add(b.get(i));
			else if (b.get(i).getClass().equals(Coin.class))
				perceptMur.add(b.get(i));
			else if (b.get(i).getClass().equals(Escalier.class))
				perceptEscalier.add(b.get(i));
			else if (b.get(i).getClass().equals(Porte.class))
				perceptPorte.add(b.get(i));
			else if (b.get(i).getClass().equals(PanneauSortie.class))
				perceptPanneau.add(b.get(i));
			else if (b.get(i).getClass().equals(Rassemblement.class))
				perceptRassemblement.add(b.get(i));
			else if (b.get(i).getClass().equals(Feu.class))
				perceptFeu.add(b.get(i));
		}
	}

	public void collision(simulateur simul)
	{
		Bag b = simul.gens.get(etage).getNeighborsExactlyWithinDistance(simul.gens.get(etage).getObjectLocation(this)/*.add(direction.multiply(vitesse))*/, simulateur.collisionDistance);
		Double2D pos = simul.gens.get(etage).getObjectLocation(this);
		Bag bmur = new Bag();
		Bag bgens = new Bag();
		Bag bescalier = new Bag();
		Bag bporte = new Bag();
		Bag bRassemblement = new Bag();
		for(int i=0;i<b.size();i++)
		{
			if(b.get(i).getClass().equals(this.getClass()))
				bgens.add(b.get(i));
			else if (b.get(i).getClass().equals(Coin.class))
				bmur.add(b.get(i));
			else if (b.get(i).getClass().equals(Escalier.class))
				bescalier.add(b.get(i));
			else if (b.get(i).getClass().equals(Porte.class))
				bporte.add(b.get(i));
			else if (b.get(i).getClass().equals(Rassemblement.class))
				bRassemblement.add(b.get(i));
		}
		// Si on percoit le centre du point de rassemblement alors on est sorti
		if(bRassemblement.size()>0)
		{
			sorti = true;
		}
		collisionType = 0;
		if(bgens.numObjs > 1)
		{
			collisionType += 1;
		}
		if(bmur.numObjs > 0)
		{
			collisionType += 2;
		}
		if(pos.y<0 || pos.x<0 || pos.y>simulateur.HAUTEUR || pos.x>simulateur.LONGUEUR)
		{
			collisionType += 2;
		}
		if(bescalier.numObjs > 0)
		{
		}
		if(bporte.numObjs > 0)
		{

		}
		collision = collisionType>0;

	}

	public Double2D nearestCollider(simulateur simul)
	{
		Double2D pos = simul.gens.get(etage).getObjectLocation(this)/*.add(direction.multiply(vitesse))*/;
		Bag b = simul.gens.get(etage).getNearestNeighbors(pos, 2, false, false, true, null);
		Double2D collider = new Double2D(-5,-5);
		double distance = 500;
		double temp;
		for(int i = 0; i < b.size();i++)
		{
			temp = pos.distance(simul.gens.get(etage).getObjectLocation(b.get(i)));
			if(temp > 0 && temp < distance)
			{
				collider = simul.gens.get(etage).getObjectLocation(b.get(i));
				distance = temp;
			}
		}
		return collider;
	}

	public void step(SimState state)
	{
		if(!sorti && !mort)
		{
			simulateur simul = (simulateur) state;
			Double2D pos = simul.gens.get(etage).getObjectLocation(this);
			if(nbTick > simulateur.nbStepsDepart)
			{
				Stats.ajoutDonneCarteChaleur((int)pos.x, (int)pos.y, (int)etage);
				if(choix == Choix.ALEATOIRE2)
					choix = Choix.ALEATOIRE;
			}
			else
			{
				choix = Choix.ALEATOIRE2;
			}

			nbTick++;


			if(random)
			{
				direction = new Double2D(1.0-2.0*simul.random.nextDouble(),1.0-2.0*simul.random.nextDouble());
				direction = direction.normalize();
				vitesse = vitesseBase;
				random=false;
			}
			if(!dansEscalier)
			{
				if(choix == Choix.ALEATOIRE)
				{
					hasObjective = false;
					if(wiggleFactor < 0.20)
						wiggleFactor = wiggleFactor * 1.01;
					else
						wiggleFactor = wiggleFactor * 0.98;
				}
				else
				{
					if(choix == Choix.ALEATOIRE2)
						wiggleFactor = 0.10;
					else
						wiggleFactor = 0.01;
				}
				currentDirection=direction;
				perception(simul);
				degatsFeu(simul);
				if(nbTick < simulateur.nbStepsDepart)
				{
					mergePercept();
				}
				realPerception(simul);
				{
					forceMurs = new Double2D(0,0);
					{ // Bloc Escalier
						double minDistance = 20;
						double distance = 0;
						boolean rapproche;
						for(int i = 0; i < perceptEscalier.size(); i++)
						{ 
							distance = pos.distance(simul.gens.get(etage).getObjectLocation(perceptEscalier.get(i)));
							if(distance < minDistance)
							{
								minDistance = distance; //On calcule la distance de l'escalier le plus proche
								int etageFinal = getEtage()+(((Escalier)perceptEscalier.get(i)).isMonte()?1:-1);
								if(Math.abs(simul.niveauSol-etageFinal) < Math.abs(simul.niveauSol-getEtage()))
									rapproche = true;
								else
									rapproche = false;
								if(Choix.ESCALIER.priorite() >= choix.priorite() && !isLocked && rapproche /*!((Escalier)perceptEscalier.get(i)).isMonte()*/)
								{
									choix = Choix.ESCALIER;
									this.hasObjective = true;
									this.objective = simul.gens.get(etage).getObjectLocation(perceptEscalier.get(i));
									objectiveDate = nbTick + 500;
								}
								else if(distance < 4 && Choix.ESCALIER2.priorite() >= choix.priorite() && !isLocked && !rapproche /*((Escalier)perceptEscalier.get(i)).isMonte()*/)
								{
									choix = Choix.ESCALIER2;
									this.hasObjective = true;
									this.objective = simul.gens.get(etage).getObjectLocation(perceptEscalier.get(i));
									objectiveDate = nbTick + 500;
								}
							}
	
							if(distance <= 2 && !isLocked && (choix == Choix.ESCALIER || choix == Choix.ESCALIER2))
							{
								//TODO: Utiliser correctement les escaliers
								if(((Escalier)perceptEscalier.get(i)).chargeActuelle <  ((Escalier)perceptEscalier.get(i)).chargeMax)
								{
									dansEscalier = true;
									tickEscalier = nbTick + ((Escalier)perceptEscalier.get(i)).tempsTraversee;
									currentEscalier = (Escalier)perceptEscalier.get(i);
									currentEscalier.chargeActuelle++;
									
									simul.gens.get(etage).remove(this);
									if(((Escalier)(perceptEscalier.get(i))).isMonte())
									{
										etage++;
										isLocked = true; //Lorsque l'on prend un escalier, on reste lock pour ne pas en reprendre un de suite
									}
									else
									{
										etage--;
										isLocked = true;
									}
									simul.gens.get(etage).setObjectLocation(this,((Escalier)(perceptEscalier.get(i))).getArrive());
									clearPercept();
									pos = simul.gens.get(etage).getObjectLocation(this);
									hasObjective = false;
									choix = Choix.ALEATOIRE;
								}	
							}
	
						}
						if(minDistance >= 7 && isLocked) //Si lock et on est a plus de 5ud (unit�s de distance) d'un escalier alors on se d�lock   
						{
							isLocked = false;
						}
					}
	
					{ // Bloc Personnes
						for(int i = 0; i < perceptPersonne.size(); i++)
						{
							double distance = pos.distance(simul.gens.get(etage).getObjectLocation(perceptPersonne.get(i))); 
							if(distance <= 2) // On ne prend en compte que les murs < 5
							{
								double multi = (25-distance*distance)/100.0;
								Double2D vector = pos.subtract(simul.gens.get(etage).getObjectLocation(perceptPersonne.get(i)));
								vector = vector.multiply(multi);
								forceMurs = forceMurs.add(vector);
							}
							if(i > 25 && wiggleFactor < 0.5)
								wiggleFactor = wiggleFactor * 1.005;
						}
					}
	
					{ // Bloc Murs
						
						
						/*for(int i = 0; i < perceptMur.size(); i++)
						{
							double distance = pos.distance(simul.gens.get(etage).getObjectLocation(perceptMur.get(i))); 
							if(distance <= 5) // On ne prend en compte que les murs < 5
							{
								double multi = (25-distance*distance)/(175.0); 
								if(hasObjective)
									multi = multi*0.65;
								Double2D vector = pos.subtract(simul.gens.get(etage).getObjectLocation(perceptMur.get(i)));
								vector = vector.multiply(multi);
								forceMurs = forceMurs.add(vector);
							}
						}
						if(forceMurs.length() > 1.5)
						{
							forceMurs = forceMurs.normalize().multiply(1.5);
						}*/
					}
	
					{ // Bloc Porte
						double distance = 0;
						double minDistance = 10;
						Double2D posPorte;
						for(int i = 0; i < perceptPorte.size(); i++)
						{
							posPorte = simul.gens.get(etage).getObjectLocation(perceptPorte.get(i));
							
							Double2D v1 = posPorte.subtract(pos).normalize();
							Double2D v2 = ((Porte)perceptPorte.get(i)).getVector();
							if(((Porte)perceptPorte.get(i)).getAngle() != -1)
							{
								double scalar = v1.getX() * v2.getX() + v1.getY() * v2.getY();
							
								distance = pos.distance(posPorte);
								if(scalar > 0)
								{
									if(distance < minDistance && choix.priorite() <= Choix.PORTE.priorite())
									{
										minDistance = distance;
										this.choix = Choix.PORTE;
										this.hasObjective = true;
										this.objective = posPorte.add(((Porte)perceptPorte.get(i)).getVector().multiply(2.5));
										objectiveDate = nbTick + 500;
									}
								}
							}
						}
					}
	
					{ // Bloc Rassemblement
						double minDistance = 20;
						double distance = 0;
						for(int i = 0; i < perceptRassemblement.size(); i++)
						{
							distance = pos.distance(simul.gens.get(etage).getObjectLocation(perceptRassemblement.get(i)));
							if(distance < minDistance && choix.priorite() <= Choix.EVACUER.priorite())
							{
								minDistance = distance;
								this.choix = Choix.EVACUER;
								this.hasObjective = true;
								this.objective = simul.gens.get(etage).getObjectLocation(perceptRassemblement.get(i));
								objectiveDate = nbTick + 500;
							}
						}
						if(minDistance <= 5) // Si on est dans la zone de rassemblement(rayon 5)
						{
							// Alors l'agent est sorti !
							sorti = true;
						}
					}
	
					{ // Bloc Panneau
						double distance = 0;
						double minDistance = 15;
						for(int i = 0; i < perceptPanneau.size(); i++)
						{
							distance = pos.distance(simul.gens.get(etage).getObjectLocation(perceptPanneau.get(i)));
							if(distance < minDistance && choix.priorite() <= Choix.INDICATION.priorite())
							{
								minDistance = distance;
								this.choix = Choix.INDICATION;
								this.hasObjective = true;
								this.objective = simul.gens.get(etage).getObjectLocation(perceptPanneau.get(i)).add(((PanneauSortie)perceptPanneau.get(i)).getVector().multiply(20));
								objectiveDate = nbTick + 500;
							}
						}
					}
	
					{ // Bloc Feu
						
					}
				}
				
				
				collision(simul);
				if(hasObjective)
					direction = objective.subtract(pos).normalize();
				if(hasObjective && nbTick > objectiveDate)
				{
					hasObjective = false;
					choix = Choix.ALEATOIRE;
				}
				if(hasObjective && pos.distance(objective) <= 5 && choix == Choix.INDICATION)
				{
					hasObjective = false;
					choix = Choix.ALEATOIRE;
				}
				if(hasObjective && pos.distance(objective) <= 1.5)
				{
					hasObjective = false;
					choix = Choix.ALEATOIRE;
				}
				//direction=direction.rotate(Math.PI/16-Math.PI/8*simul.random.nextDouble()).normalize();
				direction = direction.rotate(Math.PI*wiggleFactor/2-Math.PI*wiggleFactor*simul.random.nextDouble()).normalize();
				direction = rectifierDirection(simul, direction);
				currentDirection = direction;
				
				
				if(collision && collisionType == 1)
				{
					Double2D collider = nearestCollider(simul);
					col = collider;
					double multi = (3-(collider.distance(pos)/simulateur.collisionDistance));
					multi = multi * multi - 2.5;//multi compris entre 0.25 et 3.25
					Double2D vector = (pos.subtract(collider).multiply(multi));
					currentDirection = direction.add(vector);
					if(currentDirection.length() > 0.40)
						currentDirection = currentDirection.normalize().multiply(0.40);
					//direction = pos.subtract(collider);
					//direction.normalize();
					//direction = direction.add(pos.subtract(collider));
				}

				
				
				double vitesseMultip = vie/(double)maxVie;
				if(vitesseMultip < 0.2)
					vitesseMultip = 0.2;
				//currentDirection = currentDirection.add(forceMurs).normalize();
				//currentDirection = currentDirection.multiply(1-(forceMurs.length()/2.0));// 0.25 - 1
				
				currentDirection = rectifierDirection(simul, currentDirection);
				
				Double2D newpos = pos.add(currentDirection.multiply(vitesse*vitesseMultip));		
				if(newpos.y<0 || newpos.x<0 || newpos.y>simulateur.HAUTEUR || newpos.x>simulateur.LONGUEUR)
				{
					int i = 5;
					while(newpos.y<0 || newpos.x<0 || newpos.y>simulateur.HAUTEUR || newpos.x>simulateur.LONGUEUR)
					{
						currentDirection = currentDirection.rotate(Math.toRadians(i));
						newpos = pos.add(currentDirection.multiply(vitesse*vitesseMultip));
						if(i < 0)
							i = -(i-5);
						else
							i = -(i+5);
					}
					direction = currentDirection;
				}
				if(vie <= 0)
				{
					simul.gens.get(etage).remove(this);
					mort = true;
					simul.mort(nbTick);
				}
				else if(sorti)
				{
					simul.gens.get(etage).remove(this);
					simul.sorti(nbTick);
				}
				else
				{
					simul.gens.get(etage).setObjectLocation(this,newpos);
					distanceParcouru += newpos.distance(pos);
				}
			}
			else// Dans escalier
			{
				if(nbTick > tickEscalier)
				{
					dansEscalier = false;
					currentEscalier.chargeActuelle--;
				}
			}
		}
	}

	public int getEtage() {
		return etage;
	}

	public boolean hitObject(Object object, DrawInfo2D range)
	{
		final double SLOP = 1.0;  // need a little extra diameter to hit circles
		final double width = range.draw.width * 1.0;
		final double height = range.draw.height * 1.0;

		Ellipse2D.Double ellipse = new Ellipse2D.Double( 
				range.draw.x-width/2-SLOP, 
				range.draw.y-height/2-SLOP, 
				width+SLOP*2,
				height+SLOP*2 );
		return ( ellipse.intersects( range.clip.x, range.clip.y, range.clip.width, range.clip.height ) );
	}

	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		/*if (simulateur.afficheEtage == 0 || simulateur.afficheEtage == etage+1)
		{
			if(etage==1)
				c=Color.green;
			if (etage==0)
				c=Color.red;
			if (etage==2)
				c=Color.gray;
		    final double width = info.draw.width;
		    final double height = info.draw.height;
		    final int x = (int)(info.draw.x - width / 2.0);
		    final int y = (int)(info.draw.y - height / 2.0);
		    final int w = (int)(width);
		    final int h = (int)(height);
		    /*   
		    if (collision)
		    	graphics.setColor(Color.blue);
		    else//*
		    	graphics.setColor(c);
		    graphics.fillOval(x,y,w,h);
		    graphics.drawOval(x-(int)(w*5),y-(int)(h*5),w*10,h*10);
		}*/
		
		if (!dansEscalier && !sorti && !mort && (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1))
		{
			c=Color.red;
			final double width = info.draw.width;
			final double height = info.draw.height;
			final int x = (int)(info.draw.x - width / 2.0);
			final int y = (int)(info.draw.y - height / 2.0);
			final int w = (int)(width);
			final int h = (int)(height);
			//if (collision)
			//	c=Color.blue;

			if(hasObjective)
				c=Color.green;
			if(etage==simulateur.afficheEtage-1){
				c=c.darker().darker().darker();
			}
			graphics.setColor(c);
			
			graphics.fillOval(x,y,w,h);
			if(simulateur.affichePercept)
				graphics.drawOval(x-(int)(w*distancePerceptionInstant),y-(int)(h*distancePerceptionInstant),(int)(w*distancePerceptionInstant*2),(int)(h*distancePerceptionInstant*2));
			
			/*for(int i = 10; i > 0 ; i--)
			{
				double pv = i/10.0;
				if(i>5)
					graphics.setColor(new Color((int)(1.5-pv)*255,255,0));
				else
					graphics.setColor(new Color(255,(int)(pv)*255,0));
				double pv2 = (pv>0.20?pv:0.20)-0.20;
				graphics.setColor(new Color((int)(((2-pv2*2)>1?1:(2-pv2*2))*255),(int)(((pv2*2)>1?1:pv2*2)*255),0));
				//System.out.println("r:"+((2-pv*2)>1?1:(2-pv*2))*255+" v:"+((pv*2)>1?1:pv*2)*255);
				graphics.fillRect(x,y-h/2,(int)(w*pv),h/4);
			}*/
			if(simulateur.afficheVie)
			{
				graphics.setColor(Color.black);
				graphics.fillRect(x, y-h/2, w, h/4);
				double pv = vie/(double)maxVie;
				double pv2 = (pv>0.20?pv:0.20)-0.20;
				Color cvie = new Color((int)(((2-pv2*2)>1?1:(2-pv2*2))*255),(int)(((pv2*2)>1?1:pv2*2)*255),0);
				if(etage==simulateur.afficheEtage-1)
					cvie=cvie.darker().darker().darker();
				graphics.setColor(cvie);
				graphics.fillRect(x,y-h/2,(int)(w*pv),h/4);
			}
		}
	}
}
