package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Double2D;

public class Porte extends SimplePortrayal2D
{
	private static final long serialVersionUID = 1;
	private Double2D p;
	public int etage;
	private double angle; // en degres
	public String id;

	
	public Porte()
	{
		p = new Double2D();
		etage=0;
		angle=-1;
		id="";
	}
	
	public Porte(int etage, Double2D c, double a, String id)
	{
		this.etage=etage;
		p = new Double2D();
		p = c;
		angle=a;
		this.id=id;
	}
	
	public Porte(int etage, double x, double y, double a, String id)
	{
		this.etage= etage;
		p = new Double2D(x,y);
		angle=a;
		this.id=id;
	}

	public Double2D getVector()
	{
		double angle2 = (90 - angle)%360;
		Double2D v = new Double2D(Math.cos(Math.toRadians(angle2)),-Math.sin(Math.toRadians(angle2))).normalize();
		return v;
	}

	public int getEtage()
	{
		return etage;
	}
	public Double2D getCoord()
	{
		return p;
	}
	public double getAngle()
	{
		return angle;
	}
	public String getId()
	{
		return id;
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		if (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1)
		{
			final double width = info.draw.width*1.0;
			final double height = info.draw.height*1.0;
			
			Color c=Color.lightGray;
			if(etage==simulateur.afficheEtage-1){
		    	c=c.darker().darker().darker().darker().darker();
		    }
			
			final int x = (int)(info.draw.x - width / 2.0);
			final int y = (int)(info.draw.y - height / 2.0);
			final int w = (int)(width);
			final int h = (int)(height);
			
			// draw centered on the origin
			graphics.setColor(c);
			graphics.fillOval(x,y,w,h);
		}
	}
}
