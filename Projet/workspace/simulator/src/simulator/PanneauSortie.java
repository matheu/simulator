package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Double2D;

public class PanneauSortie extends SimplePortrayal2D
{
	private static final long serialVersionUID = 1;
	private Double2D p;
	public int etage;
	private double angle; // en degres
	public String id;

	
	public PanneauSortie()
	{
		p = new Double2D();
		etage=0;
		angle=0;
		id="";
	}
	
	public PanneauSortie(int etage, Double2D c, double a, String id)
	{
		this.etage=etage;
		p=new Double2D();
		p=c;
		angle=a;
		this.id=id;
	}
	
	public PanneauSortie(int etage, double x, double y, double a, String id)
	{
		this.etage=etage;
		p=new Double2D(x,y);
		angle=a;
		this.id=id;
	}


	public int getEtage()
	{
		return etage;
	}
	public Double2D getCoord()
	{
		return p;
	}
	public double getAngle()
	{
		return angle;
	}
	public String getId()
	{
		return id;
	}
	
	public Double2D getVector()
	{
		double angle2 = (90 - angle)%360;
		Double2D v = new Double2D(Math.cos(Math.toRadians(angle2)),-Math.sin(Math.toRadians(angle2))).normalize();
		return v;
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		if (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1)
		{
			final double width = info.draw.width*1.0;
			final double height = info.draw.height*1.0;
			
			Color c=Color.white;
			if(etage==simulateur.afficheEtage-1){
		    	c=c.darker().darker().darker().darker();
		    }
			graphics.setColor(c);
			
			//final int x = (int)(info.draw.x - width / 2.0);
			//final int y = (int)(info.draw.y - height / 2.0);
			final int x = (int)info.draw.x;
			final int y = (int)info.draw.y;
			final int w = (int)(width*0.75);
			final int h = (int)(height*0.75);
			//graphics.fillPolygon(new int[]{x, x+w, x+w/2}, new int[]{y, y, y+h}, 3);
			double theta = Math.toRadians((angle - 90)%360);
			Double2D p1 = new Double2D(w*1.5,0).rotate(theta);
			Double2D p2 = new Double2D(-w,h).rotate(theta);
			Double2D p3 = new Double2D(-w/2.0,0).rotate(theta);
			Double2D p4 = new Double2D(-w,-h).rotate(theta);
			graphics.fillPolygon(new int[]{x+(int)p1.x, x+(int)p2.x, x+(int)p3.x, x+(int)p4.x}, new int[]{y+(int)p1.y, y+(int)p2.y, y+(int)p3.y, y+(int)p4.y}, 4);
		}
	}
}
