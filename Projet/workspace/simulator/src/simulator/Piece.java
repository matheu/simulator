package simulator;

import java.awt.Color;
import java.util.Vector;

public class Piece
{
	public Color c=Color.yellow;
	Vector<Mur> murs;
	
	public Piece()
	{
		murs = new Vector<Mur>();
	}
	
	public Piece(Vector<Mur> murs)
	{
		this.murs = new Vector<Mur>();
		for(int i=0;i<murs.size();i++)
			this.murs.add(murs.get(i));
	}
	
	public int nbMur()
	{
		return murs.size();
	}
	
	public Vector<Mur> getMurs()
	{
		return murs;
	}
	
	public void addMur(Mur m)
	{
		murs.add(m);
	}
}
