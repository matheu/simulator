package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Double2D;

public class Spawn extends SimplePortrayal2D
{
	private static final long serialVersionUID = 1;
	private Double2D p;
	public int etage;
	public double probaSpawn; // Probabilite qu'une unite puisse spawn, entre 0 et 1
	public String id;

	
	public Spawn()
	{
		p = new Double2D();
		etage=0;
		id="";
		probaSpawn=1;
	}
	
	public Spawn(int etage, Double2D c, String id, double proba)
	{
		this.etage=etage;
		p = new Double2D();
		p = c;
		this.id=id;
		probaSpawn=proba;
	}
	
	public Spawn(int etage, double x, double y, String id, double proba)
	{
		this.etage= etage;
		p = new Double2D(x,y);
		this.id=id;
		probaSpawn=proba;
	}


	public int getEtage()
	{
		return etage;
	}
	public Double2D getCoord()
	{
		return p;
	}
	public String getId()
	{
		return id;
	}
	public double getProbaSpawn()
	{
		return probaSpawn;
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		if (simulateur.afficheSpawn && (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1))
		{
			final double width = info.draw.width*1.5;
			final double height = info.draw.height*1.5;
			
			Color c=Color.cyan;
			if(etage==simulateur.afficheEtage-1){
		    	c=c.darker().darker().darker().darker().darker();
		    }
			graphics.setColor(c);
			
			final int x = (int)(info.draw.x - width / 2.0);
			final int y = (int)(info.draw.y - height / 2.0);
			final int w = (int)(width);
			final int h = (int)(height);
			
			// draw centered on the origin
			graphics.fillOval(x,y,w,h);
		}
	}
}
