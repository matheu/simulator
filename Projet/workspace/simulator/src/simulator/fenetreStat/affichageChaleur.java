package simulator.fenetreStat;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class affichageChaleur extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<ArrayList<Double>> cartechaleur;

	public affichageChaleur(ArrayList<ArrayList<Double>> cartechaleur) 
	{
		this.cartechaleur = cartechaleur;

	}

	public void paintComponent(Graphics g)
	{
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		for(int i=0;i<cartechaleur.size();i++)
		{
			for(int j=0;j<cartechaleur.get(i).size();j++)
			{
				Double x = cartechaleur.get(i).get(j);
				if(x<0)
					g.setColor(Color.gray);
				else if(x>=0 && x < 0.0005)
					g.setColor(Color.blue);
				else if(0.0005 < x)
					g.setColor(getColor(x));
				g.fillRect(i*5, j*5, 5, 5);
			}
		}
	}
	
	public Color getColor(double x)
	{
		Color c = new Color(0,0,255);
		Color c2 = new Color(0,255,255);
		Color c3 = new Color(0,255,0);
		Color c4 = new Color(255,255,0);
		Color c5 = new Color(255,0,0);
		
		if(x>0.0001)
		{
			if(x<0.003)
				c=new Color(0,(int)(x*85000),255);
			else if(x<0.01)
				c=new Color(0,255,(int)(1/x*0.96-75));
			else if(x<0.05)
				c=new Color((int)(x*5100),255,0);
			else
				c=new Color(255,(int)(1/x*12),0);
		}
		
		return c;
	}
}
