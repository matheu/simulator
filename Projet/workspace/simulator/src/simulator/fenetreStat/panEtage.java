package simulator.fenetreStat;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

public class panEtage extends JPanel
{
	
	JPanel bout;
	static JButton etageinfe;
	static JButton etagesup;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public panEtage(int etage , ArrayList< ArrayList <Double> > cartechaleur)
	{
        this.setLayout(new BorderLayout());
        this.add(new affichageChaleur(cartechaleur), BorderLayout.CENTER);
        bout=new JPanel();
		etageinfe=new JButton("Etage Inferieur");
		if(etage == 0)
			etageinfe.setEnabled(false);
		else
			etageinfe.setEnabled(true);
		etagesup=new JButton("Etage Superieur");
		if(etage==fenetreCarteChaleur.nbEtage-1)
			etagesup.setEnabled(false);
		else
			etagesup.setEnabled(true);

        bout.add(etageinfe);
        bout.add(etagesup);
        this.add(bout, BorderLayout.SOUTH);
        
        etageinfe.addActionListener(new ActionListener()
		{

			public void actionPerformed(ActionEvent e)
			{
				fenetreCarteChaleur.etage--;
				CardLayout cl = (CardLayout)(fenetreCarteChaleur.fenetreChaleur.getContentPane().getLayout());
				cl.show(fenetreCarteChaleur.fenetreChaleur.getContentPane(), String.valueOf(fenetreCarteChaleur.etage));
			}
		});
        
        etagesup.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			 
			{
				fenetreCarteChaleur.etage++;
				CardLayout cl = (CardLayout)(fenetreCarteChaleur.fenetreChaleur.getContentPane().getLayout());
				cl.show(fenetreCarteChaleur.fenetreChaleur.getContentPane(), String.valueOf(fenetreCarteChaleur.etage));
			}
		});
	}
}
