package simulator.fenetreStat;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;

public class fenetreCarteChaleur extends JFrame
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int nbEtage;
	static JButton etageinfe;
	static JButton etagesup;
	static JPanel bout;
	static JFrame fenetreChaleur;
	static int etage =0;
	ArrayList<JPanel> panEtage;
	
	public fenetreCarteChaleur(ArrayList< ArrayList< ArrayList< Double > > > cartechaleur) 
	{
		fenetreCarteChaleur.
		fenetreChaleur = this;
		nbEtage = cartechaleur.size();
		this.setTitle("Carte Chaleur du batiment");
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);        
		this.setSize(cartechaleur.get(0).size()*5,cartechaleur.get(0).get(0).size()*5+60);
        panEtage = new ArrayList<JPanel>();
		this.getContentPane().setLayout(new CardLayout());
        bout=new JPanel();
		etageinfe=new JButton("Etage Inferieur");
		etageinfe.setEnabled(false);
		etagesup=new JButton("Etage Superieur");
		
        bout.add(etageinfe);
        bout.add(etagesup);
        for(int i=0;i<nbEtage;i++)
        {
        	panEtage.add(new panEtage(i,cartechaleur.get(i)));
        	this.getContentPane().add(panEtage.get(i), String.valueOf(i));	
        }
		CardLayout cl = (CardLayout)(fenetreChaleur.getContentPane().getLayout());
		cl.show(fenetreChaleur.getContentPane(), String.valueOf(etage));

	}

}
