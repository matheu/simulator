package simulator;

import java.awt.Rectangle;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.ArrayList;

import sim.engine.SimState;
import simulator.fenetreStat.fenetreCarteChaleur;
import de.progra.charting.*;
import de.progra.charting.model.*;
import de.progra.charting.render.*;

public class Stats
{
	// etage , hauteur , largeur;
	private static ArrayList< ArrayList< ArrayList< Double > > > cartechaleur;
	
	public static void resetCarteChaleur(int hauteur,int largeur,int etage)
	{
		cartechaleur = new ArrayList< ArrayList< ArrayList< Double > > >();
		for(int i=0;i<etage;i++)
		{
			cartechaleur.add(new ArrayList<ArrayList<Double> >());
			for(int j=0;j<hauteur;j++)
			{
				cartechaleur.get(i).add(new ArrayList<Double>());
				for(int k=0;k<largeur;k++)
					cartechaleur.get(i).get(j).add(0.0);
			}
		}
	}
	
	public static double getCase(int hauteur, int largeur, int etage)
	{
		return cartechaleur.get(etage).get(hauteur).get(largeur);
	}
	
	public static void initObstacleCarteChaleur(int hauteur,int largeur,int etage)
	{
		cartechaleur.get(etage).get(hauteur).set(largeur,-1.0);
	}
	
	public static void ajoutDonneCarteChaleur(int hauteur,int largeur,int etage)
	{
		if(hauteur<cartechaleur.get(0).size() && hauteur>0)
			if(largeur<cartechaleur.get(0).get(0).size() && largeur>0)
				if(cartechaleur.get(etage).get(hauteur).get(largeur)!=-1)
				{
					double add = 0.1;
					double val = cartechaleur.get(etage).get(hauteur).get(largeur);
					if(val < 500)
						add = 1;
					else if(val < 1000)
						add = 0.5;
					cartechaleur.get(etage).get(hauteur).set(largeur,cartechaleur.get(etage).get(hauteur).get(largeur)+add);
					
				}
	}
	
	public static void afficherCarteChaleur()
	{
		Double max =1.0;
		for(int i=0;i<cartechaleur.size();i++)
			for(int j=0;j<cartechaleur.get(i).size();j++)
				for(int k=0;k<cartechaleur.get(i).get(j).size();k++)
					if(cartechaleur.get(i).get(j).get(k)>max)
						max = cartechaleur.get(i).get(j).get(k);
		for(int i=0;i<cartechaleur.size();i++)
			for(int j=0;j<cartechaleur.get(i).size();j++)
				for(int k=0;k<cartechaleur.get(i).get(j).size();k++)
					if(cartechaleur.get(i).get(j).get(k)!=-1)
						cartechaleur.get(i).get(j).set(k,cartechaleur.get(i).get(j).get(k)/max);

		//System.out.println("Max: " + max);
		@SuppressWarnings("unused")
		fenetreCarteChaleur fen;
		fen = new fenetreCarteChaleur(cartechaleur);
	}
	
	public static void creerGraphique(SimState sim)
	{
		int[][] model = {{0, 1, 100}};     // Create data array
		double[] columns = {0.0, 1.0, 2000.0};  // Create x-axis values
		String[] rows = {"Nbr. pers."};          // Create data set title
		
		String title = "Recapitulatif de l'évacuation";
		
		int width = 640;                        // Image size
		int height = 480;
		
		DefaultChartDataModel data = new DefaultChartDataModel(model, columns, rows);
		
		DefaultChart c = new DefaultChart(data, title, DefaultChart.LINEAR_X_LINEAR_Y);
		  
		// Add a line chart renderer
		c.addChartRenderer(new LineChartRenderer(c.getCoordSystem(), data), 1);
		  
		// Set the chart size
		c.setBounds(new Rectangle(0, 0, width, height));
		  
		// Export the chart as a PNG image
		Random r = new Random();
		try {
			ChartEncoder.createEncodedImage(new FileOutputStream("recapEvacuation"+r.nextInt(100)+".png"), c, "png");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
