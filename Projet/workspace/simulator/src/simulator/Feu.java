package simulator;

import java.awt.Color;
import java.awt.Graphics2D;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.util.Bag;
import sim.util.Double2D;
import sim.util.Int2D;

public class Feu extends SimplePortrayal2D implements Steppable
{
	private static final long serialVersionUID = 1;
	private Int2D p;
	public int etage;
	private double probaPropage;
	private double incrChaleur;
	private final double chaleurFumeeMax = 4.5;
	private final double chaleurMax = 10;
	private boolean coinNord=false;
	private boolean coinEst=false;
	private boolean coinSud=false;
	private boolean coinOuest=false;

	private boolean feuNord=false;
	private boolean feuEst=false;
	private boolean feuSud=false;
	private boolean feuOuest=false;

	private double chaleur;
	public Bag perceptCoins;

	
	public Feu()
	{
		p = new Int2D();
		etage=0;
		chaleur = 0;
		probaPropage = 0.007+(10-simulateur.getInstance().random.nextInt(20))/20000;
		incrChaleur = 0.005+(10-simulateur.getInstance().random.nextInt(20))/15000;
	}
	
	public Feu(int etage, Int2D c)
	{
		this.etage=etage;
		p = new Int2D();
		p = c;
		chaleur = 0;
		probaPropage = 0.007+(10-simulateur.getInstance().random.nextInt(20))/20000;
		incrChaleur = 0.005+(10-simulateur.getInstance().random.nextInt(20))/15000;
	}

	public int getEtage()
	{
		return etage;
	}
	public Double2D getCoord()
	{
		return new Double2D(p.x, p.y);
	}
	public boolean estFumee()
	{
		return chaleur<=chaleurFumeeMax;
	}
	
	@Override
	public void step(SimState state)
	{
		simulateur simul = (simulateur)state;
		if(chaleur<chaleurMax)
		{
			coinNord=false;
			coinEst=false;
			coinSud=false;
			coinOuest=false;
			feuNord=false;
			feuEst=false;
			feuSud=false;
			feuOuest=false;
	
			if(p.y>0)
				feuNord=(simulateur.casesFeu[etage][p.y-1][p.x]);
			if(p.x+1<simulateur.getLONGUEUR())
				feuEst=(simulateur.casesFeu[etage][p.y][p.x+1]);
			if(p.y+1<simulateur.getHAUTEUR())
				feuSud=(simulateur.casesFeu[etage][p.y+1][p.x]);
			if(p.x>0)
				feuOuest=(simulateur.casesFeu[etage][p.y][p.x-1]);
	
			if(p.y>0)
				coinNord=(simulateur.casesCoin[etage][p.y-1][p.x]);
			if(p.x+1<simulateur.getLONGUEUR())
				coinEst=(simulateur.casesCoin[etage][p.y][p.x+1]);
			if(p.y+1<simulateur.getHAUTEUR())
				coinSud=(simulateur.casesCoin[etage][p.y+1][p.x]);
			if(p.x>0)
				coinOuest=(simulateur.casesCoin[etage][p.y][p.x-1]);
	
			if(chaleur>chaleurFumeeMax*0.1) // Propagation du feu
			{
				if(p.y>0 && !coinNord && !feuNord && simul.random.nextBoolean(probaPropage))
					simul.creerFeu(etage, p.x, p.y-1);
				if(p.x+1<simulateur.getLONGUEUR() && !coinEst && !feuEst && simul.random.nextBoolean(probaPropage))
					simul.creerFeu(etage, p.x+1, p.y);
				if(p.y+1<simulateur.getHAUTEUR() && !coinSud && !feuSud && simul.random.nextBoolean(probaPropage))
					simul.creerFeu(etage, p.x, p.y+1);
				if(p.x>0 && !coinOuest && !feuOuest && simul.random.nextBoolean(probaPropage))
					simul.creerFeu(etage, p.x-1, p.y);
			}
			
			chaleur+=incrChaleur;
		}
		else // Propagation aux �tages differents
		{
			if((etage+1)<simulateur.nbEtage) // Au dessus
			{
				if(simul.random.nextBoolean(probaPropage))
				{
					if(!simulateur.casesFeu[etage+1][p.y][p.x] && !simulateur.casesCoin[etage+1][p.y][p.x])
						simul.creerFeu(etage+1, p.x, p.y);
				}
			}
			if(etage>0) // En dessous
			{
				if(simul.random.nextBoolean(probaPropage))
				{
					if(!simulateur.casesFeu[etage-1][p.y][p.x] && !simulateur.casesCoin[etage-1][p.y][p.x])
						simul.creerFeu(etage-1, p.x, p.y);
				}
			}
		}
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		if (chaleur>0 && (simulateur.afficheEtage == etage || simulateur.afficheEtage == etage+1))
		{
			final double width = info.draw.width*1.2;
			final double height = info.draw.height*1.2;

			Color c;
			if(chaleur<chaleurFumeeMax*0.2)
			{
				double d = (chaleurFumeeMax*0.2);
				//c=Color.lightGray;
				c = new Color((int)((0.75-0.25*(chaleur/d))*255), (int)((0.75-0.25*(chaleur/d))*255),(int)((0.75-0.25*(chaleur/d))*255));
				
			}
			else if(chaleur<chaleurFumeeMax)
			{
				//c=Color.gray;
				double d = (chaleurFumeeMax-chaleurFumeeMax*0.2);
				c = new Color((int)((0.5+0.5*((chaleur-chaleurFumeeMax*0.2)/d))*255), (int)((0.5+0.28*((chaleur-chaleurFumeeMax*0.2)/d))*255), (int)((0.5-0.5*((chaleur-chaleurFumeeMax*0.2)/d))*255));
			}
			else if(chaleur<chaleurMax)
			{
				//c=Color.orange;
				double d = (chaleurMax-chaleurFumeeMax);
				c = new Color(255, (int)((0.78-0.78*((chaleur-chaleurFumeeMax)/d))*255), 0);
			}
			else
				c=Color.red;
			if(etage==simulateur.afficheEtage-1)
		    	c=c.darker().darker().darker().darker().darker();
			
			final int x = (int)(info.draw.x - width / 2.0);
			final int y = (int)(info.draw.y - height / 2.0);
			final int w = (int)(width);
			final int h = (int)(height);
			
			// draw centered on the origin
			graphics.setColor(c);
			//graphics.fillOval(x,y,w,h);
			graphics.fillRect(x,y,w,h);
		}
	}
}
