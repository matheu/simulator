package simulator;

public enum Choix 
{
	ALEATOIRE(0),	// quand il y a rien de speciale a faire on bouge aleatoirement
	ESCALIER2(2),   // quand on voit un escalier qui s'�loigne de l'�tage de base (g�n�ralement un escalier qui monte)
	PORTE(3),		// quand on vois une porte qui sort
	INDICATION(5),  // quand on voit un panneau d'indication
	ESCALIER(6),	// quand on voit un escalier (qui descend en principe)
	EVITER(8),		// quand on percoit un obstacle
	EVACUER(10),	// quand on voit un point d'evacuation
	ALEATOIRE2(11), // pendant la phase initiale
	;
   private final int priorite; // plus la priorit� est haute + l'action est importante
   
   Choix(int priorite) {
       this.priorite = priorite;
   }
   
   public int priorite() { return priorite; }
}
