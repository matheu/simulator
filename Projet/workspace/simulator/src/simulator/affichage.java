package simulator;

import sim.portrayal.Inspector;
import sim.portrayal.continuous.*;
import sim.portrayal.network.NetworkPortrayal2D;
import sim.portrayal.network.SpatialNetwork2D;
import sim.engine.*;
import sim.display.*;

import javax.swing.*;
import java.awt.Color;
import java.util.ArrayList;

public class affichage extends GUIState
{
    public Display2D display;

    public JFrame displayFrame;

    public int cpt=1000;

    ArrayList<ContinuousPortrayal2D> nodePortrayal= new ArrayList<ContinuousPortrayal2D>();
    ArrayList<NetworkPortrayal2D> edgePortrayal= new ArrayList<NetworkPortrayal2D>();

    public affichage() { super(new simulateur( System.currentTimeMillis())); }
    public affichage(SimState state) { super(state); }

    
    public static String getName() { return "Simulation d'evacuation de foule"; }

    public static Object getInfo() { 
	return "<H2>Simulation d'evacuation de foule</H2> " +
		"Simulateur permettant, a partir d'un plan batiment " +
		"ou autre, de calculer le temps d'evacuation " +
		"en fonction du nombre de personne etc...";
	}
	
	public Object getSimulationInspectedObject()
	{
		return state;
	}
	
    public Inspector getInspector()
    {
	    Inspector i = super.getInspector();
	    i.setVolatile(true);
	    return i;
    }		
    
	public void start()
	{
	    super.start();
	    setupPortrayals();
	}
	
	public void finish()
	{
		Stats.afficherCarteChaleur();
	}
	
	public void load(SimState state)
	{
	    super.load(state);
	    setupPortrayals();
	}
	
	public void setupPortrayals()
	{
		//Stats.creerGraphique(state);
		simulateur simul = (simulateur) state;
		
	    /*nodePortrayal = new ArrayList<ContinuousPortrayal2D>();
	    edgePortrayal = new ArrayList<NetworkPortrayal2D>();*/
		// tell the portrayals what to portray and how to portray them
	    for(int i=0; i<simulateur.nbEtage; i++)
	    {
			nodePortrayal.add(new ContinuousPortrayal2D());
			edgePortrayal.add(new NetworkPortrayal2D());
		    edgePortrayal.get(i).setField( new SpatialNetwork2D( simul.gens.get(i), simul.entourage ) );
		    edgePortrayal.get(i).setPortrayalForAll(new Mur());
	    	nodePortrayal.get(i).setField( simul.gens.get(i) );
	    }

		// reschedule the displayer
		display.reset();

		// redraw the display
		display.repaint();
	}
	
	public void init(Controller c)
	{
		super.init(c);
		display = new Display2D(simulateur.LONGUEUR*5,simulateur.HAUTEUR*5,this);
		displayFrame = display.createFrame();
		displayFrame.setTitle("Simulation d'evacuation");
		c.registerFrame(displayFrame);   // register the frame so it appears in the "Display" list
		displayFrame.setVisible(true);
		nodePortrayal.clear();
		edgePortrayal.clear();
		for(int i=0; i<simulateur.nbEtage; i++)
	    {
			nodePortrayal.add(new ContinuousPortrayal2D());
			edgePortrayal.add(new NetworkPortrayal2D());
			display.attach( nodePortrayal.get(i), "gens" );
		    display.attach( edgePortrayal.get(i), "murs" );
		    display.setBackdrop(Color.black);
	    }
	}
	
	public void quit()
	{
		super.quit();
	
		if (displayFrame!=null) displayFrame.dispose();
		displayFrame = null;
		display = null;
	}
	
	public static void main(String[] args)
	{
		new affichage().createController();
	}
}