package simulator;

import java.util.ArrayList;
import java.util.Vector;

import sim.engine.Schedule;
import sim.engine.Sequence;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.field.continuous.Continuous2D;
import sim.field.network.Network;
import sim.util.Double2D;
import sim.util.Int2D;


public class simulateur extends SimState
{
	public int nbgens = 250;
	public ArrayList<Continuous2D> gens;
	public Continuous2D gens1;
	public Network entourage;
	public ArrayList<Personne> personnes;
	public Double tempsMoyen;
	public Double getTempsMoyen(){ return tempsMoyen; }
	public int nbEvacues;
	public int getNbEvacues(){ return nbEvacues; }
	public int nbMorts;
	public int getNbMorts(){ return nbMorts; }
	public static simulateur Instance;
	public final static double collisionDistance = 1.5;
	public static int HAUTEUR = 100;
	public static int LONGUEUR = 100;
	public static int nbEtage;
	public static int niveauSol;
	public static int afficheEtage = 0;
	public static boolean afficheMur = true;
	public static boolean affichePercept = false;
	public static boolean afficheSpawn = false;
	public static boolean afficheDepartCatastrophe = false;
	public static boolean departIncendie = false;
	public static boolean afficheVie = true;
	public static int nbStepsDepart = 150;
	public static Vector<DepartCatastrophe> departsCatastrophes;
	public static Vector<Feu> incendies;
	public static boolean[][][] casesCoin;
	public static boolean[][][] casesFeu;
	public static Sequence sequence;
	public int lastTick;
	public int getLastTick(){ return lastTick;}
	
	private Vector<Object> objetsXML;
	
	public static int getAfficheEtage() {
		return afficheEtage;
	}

	public static void setAfficheEtage(int afficheEtage) {
		simulateur.afficheEtage = afficheEtage;
	}

	public static int getNbEtage() {
		return nbEtage;
	}

	public static void setNbEtage(int nbEtage) {
		simulateur.nbEtage = nbEtage;
	}
	

	public static int getHAUTEUR() {
		return HAUTEUR;
	}
	
	public static void setHAUTEUR(int hAUTEUR) {
		HAUTEUR = hAUTEUR;
	}

	public static int getLONGUEUR() {
		return LONGUEUR;
	}

	public static void setLONGUEUR(int lONGUEUR) {
		LONGUEUR = lONGUEUR;
	}

	public static simulateur getInstance()
	{
		return Instance;
	}
	

	public simulateur(long seed)
	{
		super(seed);
		objetsXML = JDOM.chargerBatiments();
		nbEtage=(int)objetsXML.get(0)+1;
		LONGUEUR=(int)objetsXML.get(1);
		HAUTEUR=(int)objetsXML.get(2);
		niveauSol=(int)objetsXML.get(3);
		gens = new ArrayList<Continuous2D>();
		for(int i = 0; i<nbEtage;i++)
		{
			gens.add(new Continuous2D(collisionDistance,LONGUEUR,HAUTEUR));
		}
		Stats.resetCarteChaleur(LONGUEUR, HAUTEUR, nbEtage);
		Instance=this;

	}
	
	public void start()
	{
		super.start();
		casesCoin = new boolean[nbEtage][HAUTEUR][LONGUEUR];
		casesFeu = new boolean[nbEtage][HAUTEUR][LONGUEUR];
		sequence = new Sequence(new Steppable[0]);
		tempsMoyen = 0.0;
		nbEvacues = 0;
		nbMorts = 0;
		lastTick = 0;
		gens = new ArrayList<Continuous2D>();
		personnes = new ArrayList<Personne>();
		
		for(int i = 0; i<nbEtage;i++)
		{
			gens.add(new Continuous2D(collisionDistance,LONGUEUR,HAUTEUR));
		}
		Vector<Spawn> spawns = new Vector<Spawn>();
		Vector<Rassemblement> rassemblements = new Vector<Rassemblement>();
		Vector<PanneauSortie> panneausSortie = new Vector<PanneauSortie>();
		departsCatastrophes = new Vector<DepartCatastrophe>();
		incendies = new Vector<Feu>();
		
		entourage = new Network();
		Steppable[] s = new Steppable[nbgens];

		
		// Creation des gens
		for(int i=0;i<nbgens;i++)
		{
			final Personne pers=new Personne(random.nextDouble()*0.25+0.25,random.nextInt(nbEtage),random.nextDouble()*5+10,random.nextInt(51)+75);//percept : 10-15, pv: 75-125
			personnes.add(pers);
			gens.get(pers.getEtage()).setObjectLocation(pers,new Double2D(0,0));
			entourage.addNode(pers);
			s[i] = new Steppable()
			{
				@SuppressWarnings("unused")
				private static final long serialVersionUID1 = 1;
				public void step(SimState state) { pers.step(state); }
				static final long serialVersionUID = -4269174171145445918L;
			};
			sequence.addSteppable(s[i]);
		}
		schedule.scheduleRepeating(Schedule.EPOCH,1,sequence,1);
		
		for(int i=0; i<objetsXML.size(); i++)
		{
			
			if(objetsXML.get(i).getClass()==Mur.class)
			{
				Mur m = (Mur)objetsXML.get(i);
				gens.get(m.getP1().getEtage()).setObjectLocation(m.getP1(),m.getP1().getCoord());
				gens.get(m.getP2().getEtage()).setObjectLocation(m.getP2(),m.getP2().getCoord());
				entourage.addNode(m.getP1());
				entourage.addNode(m.getP2());
				entourage.addEdge(m.getP1(),m.getP2(),null);
	
				double debx = m.getP1().getCoord().x;
				double deby = m.getP1().getCoord().y;
				double finx = m.getP2().getCoord().x;
				double finy = m.getP2().getCoord().y;
				
				double distance = Util.distanceEuclidienne(m.getP1().getCoord(), m.getP2().getCoord());
				double dx = (finx-debx)/distance;
				double dy = (finy-deby)/distance;
				boolean continu = true;
				double k = 0.0;
				while(continu)
				{
					Double2D d = new Double2D(debx+dx*k,deby+dy*k);
					gens.get(m.getP1().getEtage()).setObjectLocation(new Coin(m.getP1().getEtage()),d);
					casesCoin[m.getP1().getEtage()][(int)d.y][(int)d.x]=true;
					Stats.initObstacleCarteChaleur((int)d.x, (int)d.y, (int)m.getP1().getEtage());
					if(k<=distance-1.0)
						k+=1.0;
					else
						continu=false;
				}
			}
			else if(objetsXML.get(i).getClass()==Porte.class)
			{
				Porte p = (Porte)objetsXML.get(i);
				gens.get(p.getEtage()).setObjectLocation(p,p.getCoord());
				entourage.addNode(p);
			}
			else if(objetsXML.get(i).getClass()==Escalier.class)
			{
				Escalier e = (Escalier)objetsXML.get(i);
				gens.get(e.getEtage()).setObjectLocation(e,e.getCoord());
				entourage.addNode(e);
			}
			else if(objetsXML.get(i).getClass()==Spawn.class)
			{
				Spawn sp = (Spawn)objetsXML.get(i);
				spawns.addElement(sp);
				gens.get(sp.getEtage()).setObjectLocation(sp,sp.getCoord());
				entourage.addNode(sp);
			}
			else if(objetsXML.get(i).getClass()==Rassemblement.class)
			{
				Rassemblement r = (Rassemblement)objetsXML.get(i);
				rassemblements.addElement(r);
				gens.get(0).setObjectLocation(r,r.getCoord());
				entourage.addNode(r);
			}
			else if(objetsXML.get(i).getClass()==PanneauSortie.class)
			{
				PanneauSortie pann = (PanneauSortie)objetsXML.get(i);
				panneausSortie.addElement(pann);
				gens.get(pann.getEtage()).setObjectLocation(pann,pann.getCoord());
				entourage.addNode(pann);
			}
			else if(objetsXML.get(i).getClass()==DepartCatastrophe.class)
			{
				DepartCatastrophe d = (DepartCatastrophe)objetsXML.get(i);
				departsCatastrophes.addElement(d);
				gens.get(d.getEtage()).setObjectLocation(d,d.getCoord());
				entourage.addNode(d);
			}
		}
		/*for(int i=0;i<escaliers.size();i++)
		{
			gens.get(escaliers.get(i).getEtage()).setObjectLocation(escaliers.get(i),escaliers.get(i).getCoord());
		}
		for(int i=0;i<portes.size();i++)
		{
			gens.get(portes.get(i).getEtage()).setObjectLocation(portes.get(i),portes.get(i).getCoord());
		}
		for(int i=0;i<spawns.size();i++)
		{
			gens.get(spawns.get(i).getEtage()).setObjectLocation(spawns.get(i),spawns.get(i).getCoord());
		}
		for(int i=0;i<rassemblements.size();i++)
		{
			gens.get(0).setObjectLocation(rassemblements.get(i),rassemblements.get(i).getCoord());
		}*/
		
		// On positionne les gens sur des points de spawn et on les fait bouger un peu
		int numPointSpawn;
		boolean spawned;
		for(int i=0;i<nbgens;i++)
		{
			spawned=false;
			while(!spawned)
			{
				numPointSpawn=random.nextInt(spawns.size());
				if(random.nextDouble()<=spawns.get(numPointSpawn).getProbaSpawn())
				{
					if(personnes.get(i).getEtage()!=spawns.get(numPointSpawn).getEtage())
					{
						gens.get(personnes.get(i).getEtage()).remove(personnes.get(i));
						personnes.get(i).etage = spawns.get(numPointSpawn).getEtage();
					}
					gens.get(personnes.get(i).getEtage()).setObjectLocation(personnes.get(i),spawns.get(numPointSpawn).getCoord());
					for(int j=0; j<nbStepsDepart; j++)
					{
						personnes.get(i).step(this);
					}
					spawned=true;
				}
			}
		}
		
		if(departIncendie)
			departIncendie();
	}

	public int getNbgens()
	{
		return nbgens;
	}
	
	public void setNbgens(int nbgens)
	{
		this.nbgens = nbgens;
	}
	
	public boolean getAfficheMur()
	{
		return afficheMur;
	}
	
	public void setAfficheMur(boolean afficheMur)
	{
		simulateur.afficheMur = afficheMur;
	}
	
	public boolean getAffichePercept()
	{
		return affichePercept;
	}
	
	public void setAffichePercept(boolean affichePercept)
	{
		simulateur.affichePercept = affichePercept;
	}
	
	public boolean getAfficheSpawn()
	{
		return afficheSpawn;
	}
	
	public void setAfficheSpawn(boolean afficheSpawn)
	{
		simulateur.afficheSpawn = afficheSpawn;
	}
	
	public boolean getAfficheDepartCatastrophe()
	{
		return afficheDepartCatastrophe;
	}
	
	public void setAfficheDepartCatastrophe(boolean afficheDepartCatastrophe)
	{
		simulateur.afficheDepartCatastrophe = afficheDepartCatastrophe;
	}
	
	public boolean getAfficheVie()
	{
		return afficheVie;
	}
	
	public void setAfficheVie(boolean afficheVie)
	{
		simulateur.afficheVie = afficheVie; 
	}

	public static boolean isDepartIncendie()
	{
		return departIncendie;
	}

	public static void setDepartIncendie(boolean departIncendie)
	{
		simulateur.departIncendie = departIncendie;
		if(departIncendie && departsCatastrophes != null)
			if(!departsCatastrophes.isEmpty())
				getInstance().departIncendie();
	}
	

	public void departIncendie()
	{
		int departRandom = random.nextInt(departsCatastrophes.size());
		creerFeu(departsCatastrophes.get(departRandom).getEtage(), (int)departsCatastrophes.get(departRandom).getCoord().x, (int)departsCatastrophes.get(departRandom).getCoord().y);
	}
	
	public void creerFeu(int etage, int x, int y)
	{
		final Feu f = new Feu(etage, new Int2D(x, y));

		casesFeu[etage][y][x]=true;
		incendies.add(f);
		gens.get(etage).setObjectLocation(f,f.getCoord());
		entourage.addNode(f);
		
		Steppable s = new Steppable()
		{
			@SuppressWarnings("unused")
			private static final long serialVersionUID1 = 1;
			public void step(SimState state) { f.step(state); }
			static final long serialVersionUID = -4269174171145445918L;
		};
		sequence.addSteppable(s);
	}
	
	 // see Tutorial 3 for why this is helpful
	static final long serialVersionUID = -7164072518609011190L;
	public void sorti(int nbTick) 
	{
		nbEvacues++;
		tempsMoyen = ((tempsMoyen*(nbEvacues-1))+nbTick)/nbEvacues;
		lastTick = nbTick;
		//System.out.println(nbTick);
		
	}
	
	public void mort(int nbTick)
	{
		nbMorts++;
		//Utiliser le nb de ticks avant de mourir ?
	}
}
