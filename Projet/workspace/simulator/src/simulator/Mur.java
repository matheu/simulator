package simulator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.geom.Rectangle2D;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.network.EdgeDrawInfo2D;
import sim.portrayal.network.SimpleEdgePortrayal2D;

public class Mur extends SimpleEdgePortrayal2D
{
    private static final long serialVersionUID = 1;
	private Coin p1;
	private Coin p2;
	public final static Paint murColor = new Color(150,140,138);
	private double scale;
	public int etage;
    java.text.NumberFormat strengthFormat;
	
	transient Rectangle2D.Double preciseRectangle = new Rectangle2D.Double();
	
	public Mur()
	{
		etage = 0;
		p1 = new Coin();
		p2 = new Coin();
		scale = 4.0;
	    strengthFormat = java.text.NumberFormat.getInstance();
	    strengthFormat.setMinimumIntegerDigits(1);
	    strengthFormat.setMaximumFractionDigits(2);
	}
	
	public Mur(Coin c1, Coin c2)
	{
		p1 = new Coin();
		p2 = new Coin();
		p1 = c1;
		p2 = c2;
		etage = p1.getEtage();
		scale = 1.0;
	}
	
	public Coin getP1()
	{
		return p1;
	}
	
	public Coin getP2()
	{
		return p2;
	}
	
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
	{
		
		if (simulateur.afficheEtage == 0 || simulateur.afficheEtage == etage+1)
			if(!simulateur.afficheMur)
			{
				EdgeDrawInfo2D ei = (EdgeDrawInfo2D) info;
				/*
				final int startX = (int)(ei.draw.x>ei.secondPoint.x?ei.secondPoint.x:ei.draw.x);
				final int startY = (int)(ei.draw.y>ei.secondPoint.y?ei.secondPoint.y:ei.draw.y);
				final int endX = (int)(ei.draw.x<ei.secondPoint.x?ei.secondPoint.x:ei.draw.x);
				final int endY = (int)(ei.draw.y<ei.secondPoint.y?ei.secondPoint.y:ei.draw.y);
				/*/
				final int startX = (int)ei.draw.x;
				final int startY = (int)ei.draw.y;
				final int endX = (int)ei.secondPoint.x;
				final int endY = (int)ei.secondPoint.y;
				//*/
				final double width = (endX-startX+scale);
				final double height = (endY-startY+scale);
		
				
				graphics.setPaint(murColor);
				
				if (info.precise)
				{
					if (preciseRectangle == null) preciseRectangle= new Rectangle2D.Double();
					preciseRectangle.setFrame(info.draw.x - width/2.0, info.draw.y - height/2.0, width, height);
					graphics.fill(preciseRectangle);
					return;
				}
				
				//final int w = (int)(width);
				//final int h = (int)(height);
		
				//graphics.fillRect(startX-2,startY-2,w,h);
				//graphics.Dra
				graphics.drawLine(startX, startY, endX, endY);
			}
	}
	
	public String toString()
	{
		return p1.getCoord()+" "+p2.getCoord();
	}
}
