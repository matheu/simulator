package tuto4;
import sim.engine.*;
import sim.display.*;
import sim.portrayal.Inspector;
import sim.portrayal.LocationWrapper;
import sim.portrayal.grid.*;

import java.awt.*;

import javax.swing.*;

public class Interfacemason extends GUIState
{

	public Display2D display;
    public JFrame displayFrame;
    public Display2D display2;
    public JFrame displayFrame2;

    SparseGridPortrayal2D particlesPortrayal = new SparseGridPortrayal2D();
    SparseGridPortrayal2D particlesPortrayal2 = new SparseGridPortrayal2D();
    FastValueGridPortrayal2D trailsPortrayal = new FastValueGridPortrayal2D("Trail");

    public static void main(String[] args)
        {
        new Interfacemason().createController();
        }
    
    public Interfacemason() { super(new test3(System.currentTimeMillis())); }
    
    public Interfacemason(SimState state) { super(state); }
    
    public static String getName() { return "Tutorial3: Particles"; }
    
    public static Object getInfo()
        {
        return "<H2>Tutorial3</H2><p>An odd little particle-interaction example.";
        }
    
    public void quit()
        {
        super.quit();
        
        if (displayFrame!=null) displayFrame.dispose();
        displayFrame = null;  // let gc
        display = null;       // let gc
        }

    public void start()
        {
        super.start();
        setupPortrayals();
        // this time, we'll call display.reset() and display.repaint() in setupPortrayals()
        }
    
    public void load(SimState state)
        {
        super.load(state);
        setupPortrayals();
        // likewise...
        }
    
    public Object getSimulationInspectedObject()
    {
    return state;
    }
    
    public Inspector getInspector()
    {
    Inspector i = super.getInspector();
    i.setVolatile(true);
    return i;
    }		

    public void setupPortrayals()
    {
    // tell the portrayals what to
    // portray and how to portray them
    trailsPortrayal.setField(
    	((test3)state).trails);
    trailsPortrayal.setMap(
        new sim.util.gui.SimpleColorMap(
	    	0.0,1.0,Color.black,Color.white));
    particlesPortrayal.setField(((test3)state).particles);

    particlesPortrayal2.setField(((test3)state).particles);
    particlesPortrayal2.setPortrayalForAll(
            new sim.portrayal.simple.OvalPortrayal2D(Color.green) );
    particlesPortrayal.setPortrayalForClass(
    	    Particle.class, new sim.portrayal.simple.RectanglePortrayal2D(Color.green) );
    particlesPortrayal.setPortrayalForClass(
    	    BigParticle.class, new sim.portrayal.simple.RectanglePortrayal2D(Color.red, 1.5)
    	        {
    	        /**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				public Inspector getInspector(LocationWrapper wrapper, GUIState state)
    	            {
    	            // make the inspector
    	            return new BigParticleInspector(super.getInspector(wrapper,state), wrapper, state);
    	            }
    	        });            
    // reschedule the displayer
    display.reset();
    display2.reset();
            
    // redraw the display
    display.repaint();
    display2.repaint();
    }
    
    public void init(Controller c)
    {
    super.init(c);
    display = new Display2D(400,400,this);
    displayFrame = display.createFrame();
    c.registerFrame(displayFrame);
    displayFrame.setVisible(true);
    display.setBackdrop(Color.black);
    display.attach(trailsPortrayal,"Trails");
    display.attach(particlesPortrayal,"Particles");
    
    display2 = new Display2D(400,600,this);
    displayFrame2 = display2.createFrame();
    displayFrame2.setTitle("The Other Display");
    c.registerFrame(displayFrame2);
    displayFrame2.setVisible(true);
    display2.setBackdrop(Color.blue);
    display2.attach(particlesPortrayal2,"Squished Particles as Squares!");    }

    
}
