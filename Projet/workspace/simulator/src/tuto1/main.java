package tuto1;


public class main {

	   public static void main(String[] args)
       {
       test tutorial1 = new test(System.currentTimeMillis());
       tutorial1.start();
       long steps;
       do
           {
           if (!tutorial1.schedule.step(tutorial1))
               break;
           steps = tutorial1.schedule.getSteps();
           if (steps % 500 == 0)
               System.out.println("Steps: " + steps + " Time: " + tutorial1.schedule.getTime());
           }
       while(steps < 5000);
       tutorial1.finish();
       System.exit(0);  // make sure any threads finish up
       }
   }
