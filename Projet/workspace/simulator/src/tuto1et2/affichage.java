package tuto1et2;
import sim.engine.*;
import sim.display.*;
import sim.portrayal.grid.*;
import java.awt.*;
import javax.swing.*;

public class affichage extends GUIState
    {
    public affichage() { super(new test(System.currentTimeMillis())); }
    
    public affichage(SimState state) { super(state); }
    
    public static String getName() { return "Tutorial 2: Life"; }
    
    public static Object getInfo()
        {
        return 
        "<H2>Conway's Game of Life</H2>" +
        "<p>... with a B-Heptomino"; 
        }
    
    public Display2D display;
    public JFrame displayFrame;
    
    FastValueGridPortrayal2D gridPortrayal = new FastValueGridPortrayal2D();
    
    public void setupPortrayals()
    {
    // tell the portrayals what to portray and how to portray them
    gridPortrayal.setField(((test)state).grid);
    gridPortrayal.setMap(
        new sim.util.gui.SimpleColorMap(
        	new Color[] {new Color(0,0,0,0), Color.blue}));
    }
    
    public void start()
    {
    super.start();      
    setupPortrayals();  // set up our portrayals
    display.reset();    // reschedule the displayer
    display.repaint();  // redraw the display
    }
    
    public void init(Controller c)
    {
    super.init(c);
    
    // Make the Display2D.  We'll have it display stuff later.
    test tut = (test)state;
    display = new Display2D(tut.gridWidth * 4, tut.gridHeight * 4,this);
    displayFrame = display.createFrame();
    c.registerFrame(displayFrame);   // register the frame so it appears in the "Display" list
    displayFrame.setVisible(true);

    display.attach(gridPortrayal,"Life");  // attach the portrayals

    // specify the backdrop color  -- what gets painted behind the displays
    display.setBackdrop(Color.black);
    }
    
    public static void main(String[] args)
    {
//    affichage tutorial2 = new affichage();
//    Console c = new Console(tutorial2);
//    c.setVisible(true);
    new affichage().createController();
    }
}
    
    
    
    
    
    