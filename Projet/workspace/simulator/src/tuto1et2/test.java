package tuto1et2;

import sim.engine.*;
import sim.field.grid.*;


public class test extends SimState
{
    private static final long serialVersionUID = 1;

    public test(long seed)
    {
        super(seed);
    }
    public IntGrid2D grid;
    
    // our own parameters for setting the grid size later on
    public int gridWidth = 100;
    public int gridHeight = 100;
    
    // A b-heptomino looks like this:
    //  X
    // XXX
    // X XX
    public static final int[][] b_heptomino = new int[][]
        {{0, 1, 1},
         {1, 1, 0},
         {0, 1, 1},
         {0, 0, 1}};
    
    void seedGrid()
        {
        // we stick a b_heptomino in the center of the grid
        for(int x=0;x<b_heptomino.length;x++)
            for(int y=0;y<b_heptomino[x].length;y++)
                grid.field[x + grid.field.length/2 - b_heptomino.length/2]
                          [y + grid.field[x].length/2 - b_heptomino[x].length/2] =
                    b_heptomino[x][y];
        }
    
    public void start()
    {
    	super.start();  // very important!  This resets and cleans out the Schedule.
    	grid = new IntGrid2D(gridWidth, gridHeight);
    	seedGrid();
    	schedule.scheduleRepeating(new CA());
    }
    
    
    
}