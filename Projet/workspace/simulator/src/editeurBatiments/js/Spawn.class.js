function Spawn(x,y,z,p)
{
	this.x=x;
	this.y=y;
	this.z=z;
	this.p=p;
}

Spawn.prototype.dessine=function()
{
	buffer_context.drawImage(bufferSpawnPoint,this.x*scale-6,this.y*scale-6);
	buffer_context.fillText(this.p, this.x*scale+8, this.y*scale+6);
}