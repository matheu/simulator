var editeur,

// Variables globales pour l'optimisation
canvas = document.getElementById('cEditeur'),
buffer = document.createElement('canvas'),
context,
buffer_context,
bufferUI,
bufferUI_context,
bufferGrille,
bufferGrille_context,
bufferFlecheHaut,
bufferFlecheHaut_context,
bufferFlecheBas,
bufferFlecheBas_context,
bufferSpawnPoint,
bufferSpawnPoint_context,
bufferRassemblementPoint,
bufferRassemblementPoint_context,
bufferPanneauSortie,
bufferPanneauSortie_context,
bufferDepartCatastrophe,
bufferDepartCatastrophe_context,
bufferObstacle,
bufferObstacle_context,
tailleMap,
scale,
coords,
mouseDown=false,
lastAction=null,
temp;

function initEditeur()
{
	editeur=new Editeur();
	editeur.init();
}

function Editeur()
{
	this.init=function()
	{
		document.getElementById('batiments').value = "";
		tailleMap={x:parseInt(document.getElementById('tailleMapX').value),y:parseInt(document.getElementById('tailleMapY').value)};
		
		canvas = document.getElementById('cEditeur');
		canvas.setAttribute("width", tailleMap.x*10);
		canvas.setAttribute("height", tailleMap.y*10);
		
		document.getElementById('cadreCanvas').setAttribute("style","width:"+(tailleMap.x*10)+"px; height:"+(tailleMap.y*10)+"px;");
		
		
		context = canvas.getContext('2d');
		buffer.width = canvas.width;
		buffer.height = canvas.height;
		buffer_context = buffer.getContext('2d');
		buffer_context.font = "10px Times New Roman";
		buffer_context.fillStyle = "#B5F";
		buffer_context.strokeStyle = "#000";
		buffer_context.lineCap = 'round';
		buffer_context.lineWidth = 4;
		
		bufferUI = document.createElement('canvas');
		bufferUI.width = canvas.width;
		bufferUI.height = canvas.height;
		bufferUI_context = bufferUI.getContext('2d');
		bufferUI_context.font = "12px Times New Roman";
		bufferUI_context.fillStyle = "#B5F";
		
		bufferGrille = document.createElement('canvas');
		bufferGrille.width = canvas.width;
		bufferGrille.height = canvas.height;
		bufferGrille_context = bufferGrille.getContext('2d');
		if(!bufferGrille_context.setLineDash)
			bufferGrille_context.setLineDash=function (){}
		bufferGrille_context.strokeStyle = "#999";
		bufferGrille_context.lineWidth = 1;
		bufferGrille_context.setLineDash([1,2]);
		
		bufferFlecheHaut = document.createElement('canvas');
		bufferFlecheHaut.width = 10;
		bufferFlecheHaut.height = 10;
		bufferFlecheHaut_context = bufferFlecheHaut.getContext('2d');
		bufferFlecheHaut_context.fillStyle = "#EEE";
		bufferFlecheHaut_context.beginPath();
		bufferFlecheHaut_context.moveTo(5,2);
		bufferFlecheHaut_context.lineTo(9,8);
		bufferFlecheHaut_context.lineTo(1,8);
		bufferFlecheHaut_context.fill();
		
		bufferFlecheBas = document.createElement('canvas');
		bufferFlecheBas.width = 10;
		bufferFlecheBas.height = 10;
		bufferFlecheBas_context = bufferFlecheBas.getContext('2d');
		bufferFlecheBas_context.fillStyle = "#EEE";
		bufferFlecheBas_context.beginPath();
		bufferFlecheBas_context.moveTo(5,8);
		bufferFlecheBas_context.lineTo(9,2);
		bufferFlecheBas_context.lineTo(1,2);
		bufferFlecheBas_context.fill();
		
		
		
		bufferSpawnPoint = document.createElement('canvas');
		bufferSpawnPoint.width = 14;
		bufferSpawnPoint.height = 14;
		bufferSpawnPoint_context = bufferSpawnPoint.getContext('2d');
		bufferSpawnPoint_context.strokeStyle = "#000";
		bufferSpawnPoint_context.fillStyle = "#0FF";
		bufferSpawnPoint_context.lineWidth = 1;
		bufferSpawnPoint_context.beginPath();
		bufferSpawnPoint_context.arc(6.5,6.5,4,0,Math.PI*2,true);
		bufferSpawnPoint_context.fill();
		
		bufferSpawnPoint_context.beginPath();
		bufferSpawnPoint_context.moveTo(0.5,6.5);
		bufferSpawnPoint_context.lineTo(12.5,6.5);
		bufferSpawnPoint_context.moveTo(2.5,4.5);
		bufferSpawnPoint_context.lineTo(0.5,6.5);
		bufferSpawnPoint_context.moveTo(2.5,8.5);
		bufferSpawnPoint_context.lineTo(0.5,6.5);
		bufferSpawnPoint_context.moveTo(10.5,4.5);
		bufferSpawnPoint_context.lineTo(12.5,6.5);
		bufferSpawnPoint_context.moveTo(10.5,8.5);
		bufferSpawnPoint_context.lineTo(12.5,6.5);
		bufferSpawnPoint_context.stroke();
		
		bufferSpawnPoint_context.beginPath();
		bufferSpawnPoint_context.moveTo(6.5,0.5);
		bufferSpawnPoint_context.lineTo(6.5,12.5);
		bufferSpawnPoint_context.moveTo(4.5,2.5);
		bufferSpawnPoint_context.lineTo(6.5,0.5);
		bufferSpawnPoint_context.moveTo(8.5,2.5);
		bufferSpawnPoint_context.lineTo(6.5,0.5);
		bufferSpawnPoint_context.moveTo(4.5,10.5);
		bufferSpawnPoint_context.lineTo(6.5,12.5);
		bufferSpawnPoint_context.moveTo(8.5,10.5);
		bufferSpawnPoint_context.lineTo(6.5,12.5);
		bufferSpawnPoint_context.stroke();
		
		
		
		bufferRassemblementPoint = document.createElement('canvas');
		bufferRassemblementPoint.width = 14;
		bufferRassemblementPoint.height = 14;
		bufferRassemblementPoint_context = bufferRassemblementPoint.getContext('2d');
		bufferRassemblementPoint_context.strokeStyle = "#000";
		bufferRassemblementPoint_context.fillStyle = "#FF0";
		bufferRassemblementPoint_context.lineWidth = 1;
		bufferRassemblementPoint_context.beginPath();
		bufferRassemblementPoint_context.arc(6.5,6.5,4,0,Math.PI*2,true);
		bufferRassemblementPoint_context.fill();
		
		bufferRassemblementPoint_context.beginPath();
		bufferRassemblementPoint_context.moveTo(0.5,6.5);
		bufferRassemblementPoint_context.lineTo(4.5,6.5);
		bufferRassemblementPoint_context.moveTo(2.5,4.5);
		bufferRassemblementPoint_context.lineTo(4.5,6.5);
		bufferRassemblementPoint_context.moveTo(2.5,8.5);
		bufferRassemblementPoint_context.lineTo(4.5,6.5);
		
		bufferRassemblementPoint_context.moveTo(12.5,6.5);
		bufferRassemblementPoint_context.lineTo(8.5,6.5);
		bufferRassemblementPoint_context.moveTo(10.5,4.5);
		bufferRassemblementPoint_context.lineTo(8.5,6.5);
		bufferRassemblementPoint_context.moveTo(10.5,8.5);
		bufferRassemblementPoint_context.lineTo(8.5,6.5);
		
		bufferRassemblementPoint_context.moveTo(6.5,0.5);
		bufferRassemblementPoint_context.lineTo(6.5,4.5);
		bufferRassemblementPoint_context.moveTo(4.5,2.5);
		bufferRassemblementPoint_context.lineTo(6.5,4.5);
		bufferRassemblementPoint_context.moveTo(8.5,2.5);
		bufferRassemblementPoint_context.lineTo(6.5,4.5);
		
		bufferRassemblementPoint_context.moveTo(6.5,12.5);
		bufferRassemblementPoint_context.lineTo(6.5,8.5);
		bufferRassemblementPoint_context.moveTo(4.5,10.5);
		bufferRassemblementPoint_context.lineTo(6.5,8.5);
		bufferRassemblementPoint_context.moveTo(8.5,10.5);
		bufferRassemblementPoint_context.lineTo(6.5,8.5);
		bufferRassemblementPoint_context.stroke();
		
		
		bufferPanneauSortie = document.createElement('canvas');
		bufferPanneauSortie.width = 14;
		bufferPanneauSortie.height = 14;
		bufferPanneauSortie_context = bufferPanneauSortie.getContext('2d');
		bufferPanneauSortie_context.fillStyle = "#2A3";
		bufferPanneauSortie_context.fillRect(0,4,14,8);
		bufferPanneauSortie_context.fillStyle = "#FFF";
		bufferPanneauSortie_context.beginPath();
		bufferPanneauSortie_context.moveTo(6.5,4);
		bufferPanneauSortie_context.lineTo(10.5,10);
		bufferPanneauSortie_context.lineTo(2.5,10);
		bufferPanneauSortie_context.lineTo(6.5,4);
		bufferPanneauSortie_context.fill();
		
		
		
		bufferDepartCatastrophe = document.createElement('canvas');
		bufferDepartCatastrophe.width = 14;
		bufferDepartCatastrophe.height = 14;
		bufferDepartCatastrophe_context = bufferDepartCatastrophe.getContext('2d');
		bufferDepartCatastrophe_context.fillStyle = "#F92";
		bufferDepartCatastrophe_context.lineJoin = "round";
		bufferDepartCatastrophe_context.lineWidth = 2;
		bufferDepartCatastrophe_context.beginPath();
		bufferDepartCatastrophe_context.moveTo(6.5, 2);
		bufferDepartCatastrophe_context.lineTo(10.5, 10);
		bufferDepartCatastrophe_context.lineTo(2.5, 10);
		bufferDepartCatastrophe_context.closePath();
		bufferDepartCatastrophe_context.stroke();
		bufferDepartCatastrophe_context.fill();
		
		bufferDepartCatastrophe_context.lineWidth = 1;
		bufferDepartCatastrophe_context.beginPath();
		bufferDepartCatastrophe_context.moveTo(6.5, 4);
		bufferDepartCatastrophe_context.lineTo(6.5, 7);
		bufferDepartCatastrophe_context.stroke();
		bufferDepartCatastrophe_context.beginPath();
		bufferDepartCatastrophe_context.moveTo(6.5, 8);
		bufferDepartCatastrophe_context.lineTo(6.5, 9);
		bufferDepartCatastrophe_context.stroke();
		
		
		bufferObstacle = document.createElement('canvas');
		bufferObstacle.width = 10;
		bufferObstacle.height = 10;
		bufferObstacle_context = bufferObstacle.getContext('2d');
		bufferObstacle_context.fillStyle = "#666";
		bufferObstacle_context.fillRect(0,0,10,10);
		
		
		
		scale=Math.min(canvas.width/tailleMap.x, canvas.height/tailleMap.y);
		
		this.bufferiserGrille();

		this.coins=Array();
		this.murs=Array();
		this.obstacles=Array();
		this.portes=Array();
		this.escaliers=Array();
		this.spawns=Array();
		this.rassemblements=Array();
		this.panneauxSortie=Array();
		this.departsCatastrophes=Array();
		this.etage=0;
		this.nbrEtages=0;
		this.niveauSol=0;
		this.changerObjet(0);
		this.pointDebutEscalier=null;
		this.nbrAgents=200;

		document.onkeydown=function(event){editeur.processInput(event)};
		document.onkeyup=function(event){editeur.processInput(event)};
		document.onmousedown=function(event){editeur.processInput(event)};
		document.onmouseup=function(event){editeur.processInput(event)};
		document.onmousemove=function(event){editeur.processInput(event)};

		this.dessine(true);
	}

	this.processInput=function(event)
	{
		if(event.type=='keydown')
		{
			switch(event.keyCode)
			{
				case 65: // a
					editeur.changerObjet(5);
					break;
				case 67: // c
					editeur.changerObjet(6);
					break;
				case 68: // d
					editeur.changerObjet(3);
					break;
				case 69: // e
					editeur.changerObjet(2);
					break;
				case 73: // i
					editeur.reInit();
					break;
				case 77: // m
					editeur.changerObjet(0);
					break;
				case 79: // o
					editeur.changerObjet(7);
					break;
				case 80: // p
					editeur.changerObjet(1);
					break;
				case 82: // r
					editeur.changerObjet(4);
					break;
				case 88: // x
					editeur.chargerXML();
					break;
				case 90: // z
					editeur.annulerLastAction();
					break;
				case 107: // +
					editeur.changerEtage(1);
					break;
				case 109: // -
					editeur.changerEtage(-1);
					break;
			}
		}
		else if(event.type=='keyup')
		{
		}
		else if(event.type=='mousedown')
		{
			switch(event.button)
			{
				case 0: // Clic gauche
					mouseDown=true;
					coords=mouseCoord(event);
					break;
			}
		}
		else if(event.type=='mouseup')
		{
			switch(event.button)
			{
				case 0:
					
					break;
			}
		}
		else if(event.type=='mousemove')
		{
			editeur.majSouris(mouseCoord(event));
		}
	}
	
	this.clicCanvas=function(event)
	{
		mouseDown=false;
		var coordsFin=mouseCoord(event);
		switch(this.objetSelec)
		{
			case 0: // Mur
				editeur.ajouterMur(coords, coordsFin);
				break;
			case 1: // Porte
				editeur.ajouterSupprimerPorte(coords, coordsFin);
				break;
			case 2: // Escalier
				editeur.placerEscalier(coords);
				break;
			case 3: // Depart d'agent
				editeur.ajouterSupprimerSpawn(coords);
				break;
			case 4: // Rassemblement
				editeur.ajouterSupprimerRassemblement(coords);
				break;
			case 5: // Panneau sortie
				editeur.ajouterSupprimerPanneauSortie(coords, coordsFin);
				break;
			case 6: // Depart de catastrophe
				editeur.ajouterSupprimerDepartCatastrophe(coords);
				break;
			case 7: // Obstacle
				editeur.ajouterSupprimerObstacle(coords);
				break;
		}
	}

	this.dessine=function(ui)
	{
		buffer_context.clearRect(0, 0, buffer.width, buffer.height);
		context.clearRect(0, 0, canvas.width, canvas.height);

		buffer_context.drawImage(bufferGrille, 0, 0, bufferGrille.width, bufferGrille.height);
		
		// Dessin des objets a l'etage inferieur
		if(this.etage>0)
		{
			buffer_context.globalAlpha = 0.15;
			for(var i=0; i<this.murs.length; i++)
			{
				if(this.coins[this.murs[i].p1].z==(this.etage-1))
					this.dessineMur(i);
			}
			for(var i=0; i<this.portes.length; i++)
			{
				if(this.portes[i].z==(this.etage-1))
					this.portes[i].dessine();
			}
			for(var i=0; i<this.escaliers.length; i++)
			{
				if(this.escaliers[i].z1==(this.etage-1))
					this.escaliers[i].dessine(i,0);
				if(this.escaliers[i].z2==(this.etage-1))
					this.escaliers[i].dessine(i,1);
			}
			for(var i=0; i<this.spawns.length; i++)
			{
				if(this.spawns[i].z==(this.etage-1))
					this.spawns[i].dessine();
			}
			if(this.etage==this.niveauSol+1)
			{
				for(var i=0; i<this.rassemblements.length; i++)
					this.rassemblements[i].dessine();
			}
			for(var i=0; i<this.panneauxSortie.length; i++)
			{
				if(this.panneauxSortie[i].z==(this.etage-1))
					this.panneauxSortie[i].dessine();
			}
			for(var i=0; i<this.departsCatastrophes.length; i++)
			{
				if(this.departsCatastrophes[i].z==(this.etage-1))
					this.departsCatastrophes[i].dessine();
			}
			for(var i=0; i<this.obstacles.length; i++)
			{
				if(this.obstacles[i].z==(this.etage-1))
					this.obstacles[i].dessine();
			}
		}
		
		// Dessin des objets a l'etage courant
		buffer_context.globalAlpha = 1;
		for(var i=0; i<this.murs.length; i++)
		{
			if(this.coins[this.murs[i].p1].z==this.etage)
				this.dessineMur(i);
		}
		for(var i=0; i<this.portes.length; i++)
		{
			if(this.portes[i].z==this.etage)
				this.portes[i].dessine();
		}
		for(var i=0; i<this.escaliers.length; i++)
		{
			if(this.escaliers[i].z1==(this.etage))
				this.escaliers[i].dessine(i,0);
			if(this.escaliers[i].z2==(this.etage))
				this.escaliers[i].dessine(i,1);
		}
		for(var i=0; i<this.spawns.length; i++)
		{
			if(this.spawns[i].z==(this.etage))
				this.spawns[i].dessine();
		}
		if(this.etage==this.niveauSol)
		{
			for(var i=0; i<this.rassemblements.length; i++)
				this.rassemblements[i].dessine();
		}
		for(var i=0; i<this.panneauxSortie.length; i++)
		{
			if(this.panneauxSortie[i].z==(this.etage))
				this.panneauxSortie[i].dessine();
		}
		for(var i=0; i<this.departsCatastrophes.length; i++)
		{
			if(this.departsCatastrophes[i].z==(this.etage))
				this.departsCatastrophes[i].dessine();
		}
		for(var i=0; i<this.obstacles.length; i++)
		{
			if(this.obstacles[i].z==(this.etage))
				this.obstacles[i].dessine();
		}
		if(ui)
			buffer_context.drawImage(bufferUI, 0, 0, bufferUI.width, bufferUI.height);
		
		context.drawImage(buffer, 0, 0, buffer.width, buffer.height);
	}
	
	this.dessineMur=function(i)
	{
		buffer_context.beginPath();
		buffer_context.moveTo(this.coins[this.murs[i].p1].x*scale+0.5,this.coins[this.murs[i].p1].y*scale+0.5);
		buffer_context.lineTo(this.coins[this.murs[i].p2].x*scale+0.5,this.coins[this.murs[i].p2].y*scale+0.5);
		buffer_context.stroke();
	}
	
	this.bufferiserGrille=function()
	{
		bufferGrille_context.clearRect(0, 0, bufferGrille.width, bufferGrille.height);
		for(var i=0; i<=tailleMap.x; i++)
		{
			if(i>0 && i%5==0)
			{
				bufferGrille_context.fillText(i,i*scale-4,10);
				bufferGrille_context.beginPath();
				bufferGrille_context.moveTo(i*scale,1);
				bufferGrille_context.lineTo(i*scale,scale*tailleMap.y+1);
				bufferGrille_context.stroke();
			}
			bufferGrille_context.beginPath();
			bufferGrille_context.moveTo(i*scale+0.5,0);
			bufferGrille_context.lineTo(i*scale+0.5,scale*tailleMap.y);
			bufferGrille_context.stroke();
		}
		for(var i=0; i<=tailleMap.y; i++)
		{
			if(i>0 && i%5==0)
			{
				bufferGrille_context.fillText(i,1,i*scale+4);
				bufferGrille_context.beginPath();
				bufferGrille_context.moveTo(1,i*scale);
				bufferGrille_context.lineTo(scale*tailleMap.x+1,i*scale);
				bufferGrille_context.stroke();
			}
			bufferGrille_context.beginPath();
			bufferGrille_context.moveTo(0,i*scale+0.5);
			bufferGrille_context.lineTo(scale*tailleMap.x,i*scale+0.5);
			bufferGrille_context.stroke();
		}
	}

	this.ajouterMur=function(coords, coordsFin)
	{
		coords=this.coordToScale(coords);
		coordsFin=this.coordToScale(coordsFin);
		if(this.coordDansCadre(coords, coordsFin) && !this.coordIdentiques(coords, coordsFin))
		{
			if(!this.murExiste(coords,coordsFin) && this.getRassemblement(coords)==-1 && this.getSpawn(coords)==-1 && this.getEscalier(coords)==-1)
			{
				var	p1=this.getCoin(coords),
					p2=this.getCoin(coordsFin),
					strP1="0",
					strP2="0";
	
				if(p1==-1)
				{
					p1=this.coins.length;
					this.coins.push(new Coin(coords.x, coords.y, this.etage));
					strP1="1";
				}
				if(p2==-1)
				{
					p2=this.coins.length;
					this.coins.push(new Coin(coordsFin.x, coordsFin.y, this.etage));
					strP2="1";
				}
				
				this.murs.push(new Mur(p1,p2));
				lastAction=Array("mur",strP1,strP2);
			}
			this.dessine(false);
			this.ecritXML();
		}
	}
	
	this.ajouterSupprimerPorte=function(coords, coordsFin)
	{
		coords=this.coordToScale(coords);
		coordsFin=this.coordToScale(coordsFin);
		if(this.coordDansCadre(coords, coordsFin))
		{
			if(this.getObstacle(coords)==-1 && this.getRassemblement(coords)==-1 && this.getSpawn(coords)==-1 && this.getCoin(coords)==-1 && this.getEscalier(coords)==-1)
			{
				if(!this.porteExiste(coords))
				{
					var angle;
					if(this.coordIdentiques(coords, coordsFin))
						angle = -12;
					else
					{
						angle = -Math.acos((coordsFin.x-coords.x)/Math.sqrt(Math.pow((coordsFin.x-coords.x),2)+Math.pow((coordsFin.y-coords.y),2)));
						if(coordsFin.y>coords.y)
							angle*=-1;
						angle+=Math.PI*1/2;
					}
					this.portes.push(new Porte(coords.x, coords.y, this.etage, angle));
				}
				else
					this.portes.remove(this.getPorte(coords));
				
				this.dessine(false);
				this.ecritXML();
			}
		}
	}
	
	this.placerEscalier=function(coords)
	{
		coords=this.coordToScale(coords);
		if(this.coordDansCadre(coords, null))
		{
			if(this.getObstacle(coords)==-1 && this.getRassemblement(coords)==-1 && this.getSpawn(coords)==-1 && this.getCoin(coords)==-1 && !this.porteExiste(coords))
			{
				if(this.pointDebutEscalier==null)
				{
					this.pointDebutEscalier=new Coin(coords.x, coords.y, this.etage);
				}
				else
				{
					if(this.pointDebutEscalier.z!=this.etage)
					{
						if(this.pointDebutEscalier.z<this.etage)
							this.escaliers.push(new Escalier(this.pointDebutEscalier.x, this.pointDebutEscalier.y, this.pointDebutEscalier.z, coords.x, coords.y, this.etage));
						else
							this.escaliers.push(new Escalier(coords.x, coords.y, this.etage, this.pointDebutEscalier.x, this.pointDebutEscalier.y, this.pointDebutEscalier.z));
						this.pointDebutEscalier=null;
						this.dessine(false);
						this.ecritXML();
					}
				}
			}
		}
	}
	
	this.ajouterSupprimerSpawn=function(coords)
	{
		coords=this.coordToScale(coords);
		if(this.coordDansCadre(coords, null))
		{
			if(this.getObstacle(coords)==-1 && this.getRassemblement(coords)==-1 && this.getCoin(coords)==-1 && this.getEscalier(coords)==-1 && !this.porteExiste(coords))
			{
				if(this.getSpawn(coords)==-1)
					this.spawns.push(new Spawn(coords.x, coords.y, this.etage, document.getElementById("probaAgent").value));
				else
					this.spawns.remove(this.getSpawn(coords));
				
				this.dessine(false);
				this.ecritXML();
			}
		}
	}
	
	this.ajouterSupprimerObstacle=function(coords)
	{
		coords=this.coordToScale(coords);
		if(this.coordDansCadre(coords, null))
		{
			if(this.getSpawn(coords)==-1 && this.getRassemblement(coords)==-1 && this.getCoin(coords)==-1 && this.getEscalier(coords)==-1 && !this.porteExiste(coords))
			{
				if(this.getObstacle(coords)==-1)
					this.obstacles.push(new Obstacle(coords.x, coords.y, this.etage));
				else
					this.obstacles.remove(this.getObstacle(coords));
				
				this.dessine(false);
				this.ecritXML();
			}
		}
	}
	
	this.ajouterSupprimerRassemblement=function(coords)
	{
		coords=this.coordToScale(coords);
		if(this.coordDansCadre(coords, null) && this.etage==this.niveauSol)
		{
			if(this.getObstacle(coords)==-1 && this.getSpawn(coords)==-1 && this.getCoin(coords)==-1 && this.getEscalier(coords)==-1 && !this.porteExiste(coords))
			{
				if(this.getRassemblement(coords)==-1)
					this.rassemblements.push(new Rassemblement(coords.x, coords.y));
				else
					this.rassemblements.remove(this.getRassemblement(coords));
				
				this.dessine(false);
				this.ecritXML();
			}
		}
	}
	
	this.ajouterSupprimerPanneauSortie=function(coords, coordsFin)
	{
		coords=this.coordToScale(coords);
		coordsFin=this.coordToScale(coordsFin);
		if(this.coordDansCadre(coords, coordsFin) && !this.coordIdentiques(coords, coordsFin))
		{
			if(this.getObstacle(coords)==-1 && this.getCoin(coords)==-1 && this.getEscalier(coords)==-1)
			{
				if(this.getPanneauSortie(coords)==-1)
				{
					var angle = -Math.acos((coordsFin.x-coords.x)/Math.sqrt(Math.pow((coordsFin.x-coords.x),2)+Math.pow((coordsFin.y-coords.y),2)));
					if(coordsFin.y>coords.y)
						angle*=-1;
					this.panneauxSortie.push(new PanneauSortie(coords.x, coords.y, this.etage, angle+Math.PI*1/2));
				}
				else
					this.panneauxSortie.remove(this.getPanneauSortie(coords));
				
				this.dessine(false);
				this.ecritXML();
			}
		}
	}
	
	this.ajouterSupprimerDepartCatastrophe=function(coords)
	{
		coords=this.coordToScale(coords);
		if(this.coordDansCadre(coords, null))
		{
			if(this.getDepartCatastrophe(coords)==-1)
				this.departsCatastrophes.push(new DepartCatastrophe(coords.x, coords.y, this.etage, "feu", document.getElementById("probaCata").value));
			else
				this.departsCatastrophes.remove(this.getDepartCatastrophe(coords));
			
			this.dessine(false);
			this.ecritXML();
		}
	}
	
	this.ecritXML=function()
	{
		var	strCoins='\t<coins>\n',
			strMurs='',
			strPortes='',
			strEscaliers='',
			strSpawns='',
			strRassemblements='',
			strPanneauxSortie='',
			strDepartsCatastrophes='';
			strObstacles='';
		this.nbrEtages=this.compterEtages();
		var str='<?xml version="1.0" encoding="UTF-8"?>\n\n<batiment id="b1" tailleX="'+tailleMap.x+'" tailleY="'+tailleMap.y+'" etages="'+this.nbrEtages+'" sol="'+this.niveauSol+'" nbrAgents="'+this.nbrAgents+'">\n';
		var i;
		for(i=0; i<this.coins.length; i++)
		{
			if(!this.coins[i].aEteEcrit)
			{
				strCoins+='\t\t<coin id="coin'+i+'" x="'+this.coins[i].x+'" y="'+this.coins[i].y+'" z="'+this.coins[i].z+'" />\n';
			}
		}
		for(i=0; i<this.murs.length; i++)
		{
			strMurs+='\t\t<mur id="mur'+i+'" p1="coin'+this.murs[i].p1+'" p2="coin'+this.murs[i].p2+'" />\n';
		}
		for(i=0; i<this.portes.length; i++)
		{
			
			temp=this.portes[i].angle==-12?-1:Math.round(this.portes[i].angle/Math.PI*180+360)%360;
			strPortes+='\t\t<porte id="port'+i+'" x="'+this.portes[i].x+'" y="'+this.portes[i].y+'" z="'+this.portes[i].z+'" angle="'+(temp)+'" />\n';
		}
		for(i=0; i<this.escaliers.length; i++)
		{
			strEscaliers+='\t\t<escalier id="esca'+i+'" x1="'+this.escaliers[i].x1+'" y1="'+this.escaliers[i].y1+'" z1="'+this.escaliers[i].z1+'" x2="'+this.escaliers[i].x2+'" y2="'+this.escaliers[i].y2+'" z2="'+this.escaliers[i].z2+'" />\n';
		}
		for(i=0; i<this.spawns.length; i++)
		{
			strSpawns+='\t\t<spawn id="spwn'+i+'" x="'+this.spawns[i].x+'" y="'+this.spawns[i].y+'" z="'+this.spawns[i].z+'" proba="'+this.spawns[i].p+'" />\n';
		}
		for(i=0; i<this.rassemblements.length; i++)
		{
			strRassemblements+='\t\t<rassemblement id="rass'+i+'" x="'+this.rassemblements[i].x+'" y="'+this.rassemblements[i].y+'" />\n';
		}
		for(i=0; i<this.panneauxSortie.length; i++)
		{
			temp=this.panneauxSortie[i].angle/Math.PI;
			strPanneauxSortie+='\t\t<panneauSortie id="pann'+i+'" x="'+this.panneauxSortie[i].x+'" y="'+this.panneauxSortie[i].y+'" z="'+this.panneauxSortie[i].z+'" angle="'+(Math.round(temp*180+360)%360)+'" />\n';
		}
		for(i=0; i<this.departsCatastrophes.length; i++)
		{
			strDepartsCatastrophes+='\t\t<departCatastrophe id="cata'+i+'" x="'+this.departsCatastrophes[i].x+'" y="'+this.departsCatastrophes[i].y+'" z="'+this.departsCatastrophes[i].z+'" type="'+this.departsCatastrophes[i].type+'" proba="'+this.departsCatastrophes[i].p+'" />\n';
		}
		for(i=0; i<this.obstacles.length; i++)
		{
			strObstacles+='\t\t<obstacle id="obst'+i+'" x="'+this.obstacles[i].x+'" y="'+this.obstacles[i].y+'" z="'+this.obstacles[i].z+'" />\n';
		}
		str+=strCoins+"\t</coins>\n";
		str+='\t<piece id="p1">\n'+strMurs+"\n"+strPortes+"\n"+strEscaliers+"\n"+strSpawns+"\n"+strRassemblements+"\n"+strPanneauxSortie+"\n"+strDepartsCatastrophes+"\n"+strObstacles+"\t</piece>\n";
		str+="</batiment>";
		document.getElementById('batiments').value = str;
	}
	
	this.chargerXML=function()
	{
		if(document.getElementById('batiments').value!="")
		{
			var xmlDoc = parseXml(document.getElementById('batiments').value);
			document.getElementById('tailleMapX').value=xmlDoc.getElementsByTagName("batiment")[0].attributes.getNamedItem("tailleX").nodeValue;
			document.getElementById('tailleMapY').value=xmlDoc.getElementsByTagName("batiment")[0].attributes.getNamedItem("tailleY").nodeValue;
			this.reInit();
			
			this.niveauSol=xmlDoc.getElementsByTagName("batiment")[0].attributes.getNamedItem("sol").nodeValue;
			this.nbrAgents=xmlDoc.getElementsByTagName("batiment")[0].attributes.getNamedItem("nbrAgents").nodeValue;
			document.getElementById('nbrAgents').value=this.nbrAgents;
			
			var lesCoins = xmlDoc.getElementsByTagName("batiment")[0].getElementsByTagName("coins")[0];
			for(var i=1; i<lesCoins.childNodes.length-1; i+=2)
			{
				this.coins.push(new Coin(
					parseInt(lesCoins.childNodes[i].attributes.getNamedItem("x").nodeValue),
					parseInt(lesCoins.childNodes[i].attributes.getNamedItem("y").nodeValue),
					parseInt(lesCoins.childNodes[i].attributes.getNamedItem("z").nodeValue)
				));
			}
			
			var lesObjets = xmlDoc.getElementsByTagName("batiment")[0].getElementsByTagName("piece")[0];
			for(var i=1; i<lesObjets.childNodes.length-1; i+=2)
			{
				switch(lesObjets.childNodes[i].nodeName)
				{
					case "mur":
						var indiceCoin1=1;
						while(indiceCoin1<lesCoins.childNodes.length-1 && lesCoins.childNodes[indiceCoin1].attributes.getNamedItem("id").nodeValue!=lesObjets.childNodes[i].attributes.getNamedItem("p1").nodeValue)
							indiceCoin1+=2;
						
						var indiceCoin2=1;
						while(indiceCoin2<lesCoins.childNodes.length-1 && lesCoins.childNodes[indiceCoin2].attributes.getNamedItem("id").nodeValue!=lesObjets.childNodes[i].attributes.getNamedItem("p2").nodeValue)
							indiceCoin2+=2;
						
						this.murs.push(new Mur(
							(indiceCoin1-1)/2,
							(indiceCoin2-1)/2
						));
						break;
					case "porte":
						temp=parseInt(lesObjets.childNodes[i].attributes.getNamedItem("angle").nodeValue);
						temp=temp==-1?-12:temp*Math.PI/180;
						this.portes.push(new Porte(
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("z").nodeValue),
							temp
						));
						break;
					case "escalier":
						this.escaliers.push(new Escalier(
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x1").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y1").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("z1").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x2").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y2").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("z2").nodeValue)
						));
						break;
					case "spawn":
						this.spawns.push(new Spawn(
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("z").nodeValue),
							parseFloat(lesObjets.childNodes[i].attributes.getNamedItem("proba").nodeValue)
						));
						break;
					case "rassemblement":
						this.rassemblements.push(new Rassemblement(
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y").nodeValue)
						));
						break;
					case "panneauSortie":
						this.panneauxSortie.push(new PanneauSortie(
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("z").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("angle").nodeValue)*Math.PI/180
						));
						break;
					case "departCatastrophe":
						this.departsCatastrophes.push(new DepartCatastrophe(
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("z").nodeValue),
							lesObjets.childNodes[i].attributes.getNamedItem("type").nodeValue,
							parseFloat(lesObjets.childNodes[i].attributes.getNamedItem("proba").nodeValue)
						));
						break;
					case "obstacle":
						this.obstacles.push(new Obstacle(
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("x").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("y").nodeValue),
							parseInt(lesObjets.childNodes[i].attributes.getNamedItem("z").nodeValue)
						));
						break;
				}
			}
			
			this.dessine(false);
			this.ecritXML();
		}
	}

	this.majSouris=function(coordsActuelles)
	{
		bufferUI_context.clearRect(0, 0, bufferUI.width, bufferUI.height);
		context.clearRect(0, 0, canvas.width, canvas.height);
		context.drawImage(buffer, 0, 0, buffer.width, buffer.height);
		if(mouseDown && this.coordDansCadre({x:Math.round(coords.x/scale),y:Math.round(coords.y/scale)},null))
		{
			switch(this.objetSelec)
			{
				case 0:
					bufferUI_context.beginPath();
					bufferUI_context.moveTo(Math.round(coords.x/scale)*scale+0.5,Math.round(coords.y/scale)*scale+0.5);
					bufferUI_context.lineTo(Math.round(coordsActuelles.x/scale)*scale+0.5,Math.round(coordsActuelles.y/scale)*scale+0.5);
					bufferUI_context.stroke();
					break;
				case 5:
					bufferUI_context.beginPath();
					bufferUI_context.moveTo(Math.round(coords.x/scale)*scale+0.5,Math.round(coords.y/scale)*scale+0.5);
					bufferUI_context.lineTo(Math.round(coordsActuelles.x/scale)*scale+0.5,Math.round(coordsActuelles.y/scale)*scale+0.5);
					bufferUI_context.stroke();
					break;
			}
		}
		bufferUI_context.fillText(Math.round(coordsActuelles.x/scale)+";"+Math.round(coordsActuelles.y/scale), coordsActuelles.x, coordsActuelles.y);

		context.drawImage(bufferUI, 0, 0, bufferUI.width, bufferUI.height);
		
		//document.getElementById('cadreInputNombre').setAttribute("style","top:"+(coordsActuelles.y+canvas.offsetTop-13)+"px; left:"+(coordsActuelles.x+canvas.offsetLeft-30)+"px;");
	}

	this.getCoin=function(coord)
	{
		var i=0;
		while(i<this.coins.length)
		{
			if(this.coins[i].x==coord.x && this.coins[i].y==coord.y && this.coins[i].z==this.etage)
				return i;
			i++;
		}
		return -1;
	}
	
	this.nbrMursUtilisentPoint=function(p)
	{
		var cpt=0;
		for(var i=0; i<this.murs.length; i++)
		{
			if((this.coins[this.murs[i].p1].x==p.x && this.coins[this.murs[i].p1].y==p.y) || (this.coins[this.murs[i].p2].x==p.x && this.coins[this.murs[i].p2].y==p.y))
				cpt++;
		}
		return cpt;
	}

	this.murExiste=function(coords,coordsFin)
	{
		var trouve=false, i=0;
		while(i<this.coins.length && !trouve)
		{
			trouve=(this.coins[i].x==coords.x && this.coins[i].y==coords.y && this.coins[i].z==this.etage);
			i++;
		}
		if(trouve)
		{
			trouve=false;
			i=0;
			while(i<this.coins.length && !trouve)
			{
				trouve=(this.coins[i].x==coordsFin.x && this.coins[i].y==coordsFin.y && this.coins[i].z==this.etage);
				i++;
			}
			if(trouve)
			{
				trouve=false;
				i=0;
				while(i<this.murs.length && !trouve)
				{
					trouve=((this.coins[this.murs[i].p1].x==coords.x && this.coins[this.murs[i].p1].y==coords.y && this.coins[this.murs[i].p2].x==coordsFin.x && this.coins[this.murs[i].p2].y==coordsFin.y && this.coins[this.murs[i].p1].z==this.etage && this.coins[this.murs[i].p2].z==this.etage) || (this.coins[this.murs[i].p1].x==coordsFin.x && this.coins[this.murs[i].p1].y==coordsFin.y && this.coins[this.murs[i].p2].x==coords.x && this.coins[this.murs[i].p2].y==coords.y && this.coins[this.murs[i].p1].z==this.etage && this.coins[this.murs[i].p2].z==this.etage));
					i++;
				}
			}
		}
		
		return trouve;
	}

	this.getMur=function(p1,p2)
	{
		var trouve=false, i=0;
		while(i<this.murs.length && !trouve)
		{
			trouve=(this.murs[i].p1==p1 && this.murs[i].p2==p2);
			if(!trouve)
				i++;
		}
		
		return trouve?i:-1;
	}

	this.porteExiste=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.portes.length && !trouve)
		{
			trouve=(this.portes[i].x==coords.x && this.portes[i].y==coords.y && this.portes[i].z==this.etage);
			i++;
		}
		return trouve;
	}

	this.getPorte=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.portes.length && !trouve)
		{
			trouve=(this.portes[i].x==coords.x && this.portes[i].y==coords.y && this.portes[i].z==this.etage);
			if(!trouve)
				i++;
		}
		return trouve?i:-1;
	}

	this.getObstacle=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.obstacles.length && !trouve)
		{
			trouve=(this.obstacles[i].x==coords.x && this.obstacles[i].y==coords.y && this.obstacles[i].z==this.etage);
			if(!trouve)
				i++;
		}
		return trouve?i:-1;
	}

	this.getEscalier=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.escaliers.length && !trouve)
		{
			trouve=((this.escaliers[i].x1==coords.x && this.escaliers[i].y1==coords.y && this.escaliers[i].z1==this.etage) || (this.escaliers[i].x2==coords.x && this.escaliers[i].y2==coords.y && this.escaliers[i].z2==this.etage));
			if(!trouve)
				i++;
		}
		return trouve?i:-1;
	}

	this.getSpawn=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.spawns.length && !trouve)
		{
			trouve=(this.spawns[i].x==coords.x && this.spawns[i].y==coords.y && this.spawns[i].z==this.etage);
			if(!trouve)
				i++;
		}
		return trouve?i:-1;
	}

	this.getRassemblement=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.rassemblements.length && !trouve)
		{
			trouve=(this.rassemblements[i].x==coords.x && this.rassemblements[i].y==coords.y && this.etage==0);
			if(!trouve)
				i++;
		}
		return trouve?i:-1;
	}

	this.getPanneauSortie=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.panneauxSortie.length && !trouve)
		{
			trouve=(this.panneauxSortie[i].x==coords.x && this.panneauxSortie[i].y==coords.y && this.panneauxSortie[i].z==this.etage);
			if(!trouve)
				i++;
		}
		return trouve?i:-1;
	}

	this.getDepartCatastrophe=function(coords)
	{
		var trouve=false, i=0;
		while(i<this.departsCatastrophes.length && !trouve)
		{
			trouve=(this.departsCatastrophes[i].x==coords.x && this.departsCatastrophes[i].y==coords.y && this.departsCatastrophes[i].z==this.etage);
			if(!trouve)
				i++;
		}
		return trouve?i:-1;
	}

	this.coordToScale=function(coord)
	{
		return {x:Math.round(coord.x/scale),y:Math.round(coord.y/scale)};
	}
	
	this.changerEtage=function(incr)
	{
		this.etage+=incr;
		if(this.etage<0)
			this.etage=0;
		if(this.etage>99)
			this.etage=99;
		document.getElementById('etage').innerHTML = this.etage;
		this.nbrEtages=this.compterEtages();
		this.dessine(false);
	}
	
	this.changerSol=function(incr)
	{
		this.niveauSol+=incr;
		if(this.niveauSol<0)
			this.niveauSol=0;
		if(this.niveauSol>this.nbrEtages)
			this.niveauSol=this.nbrEtages;
		document.getElementById('sol').innerHTML = this.niveauSol;
		this.dessine(false);
		this.ecritXML();
	}
	
	this.compterEtages=function()
	{
		var cpt=0;
		for(var i=0; i<this.coins.length; i++)
			cpt=Math.max(cpt,this.coins[i].z);
		for(var i=0; i<this.portes.length; i++)
			cpt=Math.max(cpt,this.portes[i].z);
		for(var i=0; i<this.escaliers.length; i++)
			cpt=Math.max(cpt,Math.max(this.escaliers[i].z1,this.escaliers[i].z2));
		for(var i=0; i<this.spawns.length; i++)
			cpt=Math.max(cpt,this.spawns[i].z);
		return cpt;
	}
	
	this.changerObjet=function(num)
	{
		this.objetSelec=num;
		for(var i=0; i<8; i++)
			document.getElementById('objet'+i).disabled = false;
		document.getElementById('objet'+num).disabled = true;
	}
	
	this.annulerLastAction=function()
	{
		if(lastAction!=null)
		{
			if(lastAction[0]=="mur")
			{
				if(lastAction[1]=="1")
					this.coins.remove(this.murs[this.murs.length-1].p1);
				if(lastAction[2]=="1")
					this.coins.remove(this.murs[this.murs.length-1].p2);
				this.murs.remove(this.murs.length-1);
			}
		}
		lastAction=null;
		this.dessine(false);
		this.ecritXML();
	}
	
	this.setNbrAgents=function()
	{
		this.nbrAgents=parseInt(document.getElementById('nbrAgents').value);
		this.ecritXML();
	}

	this.reInit=function()
	{
		context.clearRect(0, 0, canvas.width, canvas.height);
		editeur.init();
	}
	
	this.coordDansCadre=function(coords, coordsFin)
	{
		if(coordsFin==null)
			return (coords.x>0 && coords.y>0 && coords.x<=tailleMap.x && coords.y<=tailleMap.y);
		else
			return (coords.x>0 && coords.y>0 && coords.x<=tailleMap.x && coords.y<=tailleMap.y && coordsFin.x>0 && coordsFin.y>0 && coordsFin.x<=tailleMap.x && coordsFin.y<=tailleMap.y);
	}
	
	this.coordIdentiques=function(coords, coordsFin)
	{
		return (coords.x==coordsFin.x && coords.y==coordsFin.y);
	}
}