function Escalier(x1,y1,z1,x2,y2,z2)
{
	this.x1=x1;
	this.y1=y1;
	this.z1=z1;
	this.x2=x2;
	this.y2=y2;
	this.z2=z2;
}

Escalier.prototype.dessine=function(num,z)
{
	if(z==0)
	{
		buffer_context.fillRect(this.x1*scale-5, this.y1*scale-5, 10, 10);
		buffer_context.drawImage(bufferFlecheHaut, this.x1*scale-5, this.y1*scale-5, 10, 10);
		buffer_context.fillText(num, this.x1*scale+6, this.y1*scale);
	}
	else
	{
		buffer_context.fillRect(this.x2*scale-5, this.y2*scale-5, 10, 10);
		buffer_context.drawImage(bufferFlecheBas, this.x2*scale-5, this.y2*scale-5, 10, 10);
		buffer_context.fillText(num, this.x2*scale+6, this.y2*scale);
	}
}