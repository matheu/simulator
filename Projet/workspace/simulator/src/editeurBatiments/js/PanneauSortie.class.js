function PanneauSortie(x,y,z,a)
{
	this.x=x;
	this.y=y;
	this.z=z;
	this.angle=a;
}

PanneauSortie.prototype.dessine=function()
{
	buffer_context.save();
	buffer_context.translate(this.x*scale,this.y*scale);
	buffer_context.rotate(this.angle);
	buffer_context.translate(-this.x*scale,-this.y*scale);
	buffer_context.drawImage(bufferPanneauSortie,this.x*scale-7,this.y*scale-7);
	buffer_context.restore();
}