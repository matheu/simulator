mouseCoord=function(e)
{
	e = e || window.event;
	var element=canvas;
	var pageX = e.pageX-element.offsetLeft;
	var pageY = e.pageY-element.offsetTop;
	while(element=element.offsetParent)
	{
		pageX -= element.offsetLeft;
		pageY -= element.offsetTop;
	}
	if (pageX === undefined) {
		pageX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		pageY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	return {x:pageX,y:pageY};
}

Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};


if (typeof window.DOMParser != "undefined") {
    parseXml = function(xmlStr) {
        return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
    };
} else if (typeof window.ActiveXObject != "undefined" &&
       new window.ActiveXObject("Microsoft.XMLDOM")) {
    parseXml = function(xmlStr) {
        var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(xmlStr);
        return xmlDoc;
    };
} else {
    throw new Error("No XML parser found");
}