function DepartCatastrophe(x,y,z,t,p)
{
	this.x=x;
	this.y=y;
	this.z=z;
	this.type=t;
	this.p=p;
}

DepartCatastrophe.prototype.dessine=function()
{
	buffer_context.drawImage(bufferDepartCatastrophe,this.x*scale-6,this.y*scale-6);
	buffer_context.fillText(this.p, this.x*scale+6, this.y*scale+6);
}