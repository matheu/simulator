function Porte(x,y,z,a)
{
	this.x=x;
	this.y=y;
	this.z=z;
	this.angle=a;
}

Porte.prototype.dessine=function()
{
	if(this.angle!=-12)
	{
		buffer_context.beginPath();
		buffer_context.moveTo(this.x*scale, this.y*scale);
		buffer_context.lineTo(-10*Math.cos(this.angle+Math.PI/2)+this.x*scale, -10*Math.sin(this.angle+Math.PI/2)+this.y*scale);
		buffer_context.stroke();
	}
	buffer_context.fillRect(this.x*scale-5, this.y*scale-5, 10, 10);
}