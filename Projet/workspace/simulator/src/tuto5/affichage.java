package tuto5;

import sim.portrayal.network.*;
import sim.portrayal.continuous.*;
import sim.engine.*;
import sim.display.*;

import javax.swing.*;
import java.awt.Color;

public class affichage extends GUIState
    {
    public Display2D display;
    public JFrame displayFrame;

    NetworkPortrayal2D edgePortrayal = new NetworkPortrayal2D();
    ContinuousPortrayal2D nodePortrayal = new ContinuousPortrayal2D();

    public static void main(String[] args)
        {
        new affichage().createController();
        }

    public affichage() { super(new test4( System.currentTimeMillis())); }
    public affichage(SimState state) { super(state); }

    public static String getName() { return "Tutorial 5: Hooke's Law"; }
    
    public static Object getInfo() { return "<H2>Tutorial 5</H2> Hooke's Law"; }

    public Object getSimulationInspectedObject() { return state; }

    public void start()
    {
        super.start();
        setupPortrayals();
    }

    public void load(SimState state)
        {
        super.load(state);
        setupPortrayals();
        }
    public void setupPortrayals()
    {
    test4 tut = (test4) state;
    
    // tell the portrayals what to portray and how to portray them
    edgePortrayal.setField( new SpatialNetwork2D( tut.balls, tut.bands ) );
    edgePortrayal.setPortrayalForAll(new BandPortrayal2D());
    nodePortrayal.setField( tut.balls );

    // reschedule the displayer
    display.reset();
    display.setBackdrop(Color.white);

    // redraw the display
    display.repaint();
    }
    public void init(Controller c)
    {
    super.init(c);

    // make the displayer
    display = new Display2D(600,600,this);
 // turn off clipping
    display.setClipping(false);

    displayFrame = display.createFrame();
    displayFrame.setTitle("Tutorial 5 Display");
    c.registerFrame(displayFrame);   // register the frame so it appears in the "Display" list
    displayFrame.setVisible(true);
    display.attach( edgePortrayal, "Bands" );
    display.attach( nodePortrayal, "Balls" );
    }

public void quit()
    {
    super.quit();

    if (displayFrame!=null) displayFrame.dispose();
    displayFrame = null;
    display = null;
    }
}